local I, C, L = unpack(select(2, ...))

C["Filger_Config"] = {
	["cleverzone"] = false,		-- load only PVP in PVP zones and PVE in PVE zones (require to reload the 1st time you enter the pve zone)
	["tooltip"] = true,			-- tooltip on mouseover buffs, some people wanted it. I don't.
	["TooltipMover"] = true,	-- tooltip on TooltipMover for ElvUI and Tukui users.
	["FlashIcon"] = true,		-- Flash when time left is below a threshold in ICON mode.
	["FlashBar"] = false,		-- Flash when time left is below a threshold in BAR mode.
	["FlashThreshold"] = 5,		-- Threshold from which icons start flashing.
	["FlashDuration"] = 0.5,	-- Duration of each flash smaller => quicker.
	["autoupdate"] = false,		-- Automaticaly update the config.
	["Config Version"] = {		-- To warn thoses who use ingame Config that their database is outdated.
								-- We need to filter the class, if MAGE is updated but not DK why should we make a reset for the DK ???
		["MAGE"] = 1.1,
		["DEATHKNIGHT"] = 1,
		["PRIEST"] = 1,
		["WARLOCK"] = 1,
		["DRUID"] = 1,
		["SHAMAN"] = 1.1,
		["HUNTER"] = 1,
		["WARRIOR"] = 1,
		["ROGUE"] = 1,
		["PALADIN"] = 1.2,
		["MONK"] = 1,
		["ALL"] = 1.3,
	}
}

C["Filger_Cooldown"] = {
	["enable"] = true,
	["treshold"] = 8,	-- show decimal under X seconds and text turn red
	["fontsize"] = 20,	--the base font size to use at a scale of 1
}

--	List of available options for Panels:
--		name	= "name of the panel" (this name will be used in Spells options part where you specify what panel do you want to attach your icons (or bars) to)
--		w		= width of the panel in pixels
--		h		= height of the panel in pixels
--		anchor	= what point of the panel is used to determine panels position ("TOP" / "BOTTOM" / "LEFT" / "RIGHT" / "CENTER" / "TOPLEFT" / "TOPRIGHT" / "BOTTOMLEFT" / "BOTTOMRIGHT"
--		x		= default anchor's position relative to the center of the screen, horizontal, in pixels
--		y		= default anchor's position relative to the center of the screen, vertical, in pixels
--		text	= label text that will be displayed on the panel

C["Filger_Panels"] = {
	["MAGE"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerMageInvertAura", w = 200, h = 20, anchor = "BOTTOM", x = 0, y = 30, text = "Invert Auras/CD" },
		{ name = "iFilgerInterrupt", w = 75, h = 20, anchor = "BOTTOM", x = 0, y = 10, text = "Interrupt" },
	},
	["DEATHKNIGHT"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
	},
	["PRIEST"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
		{ name = "iFilgerDebuffDps", w = 200, h = 20, anchor = "TOPLEFT", x = 30, y = -210, text = "Debuff Dps" },
		{ name = "iFilgerBuffDps", w = 200, h = 20, anchor = "TOPRIGHT", x = -30, y = -210, text = "Buff/Procs Dps" },
		{ name = "iFilgerProcDps", w = 200, h = 20, anchor = "CENTER", x = 0, y = -250, text = "CD Dps" },
		{ name = "iFilgerBuffDebuffHeal", w = 200, h = 20, anchor = "BOTTOMRIGHT", x = 98, y = -61, text = "Buff / Debuff Heal" },
		{ name = "iFilgerBuffPlayerHeal", w = 200, h = 20, anchor = "TOPRIGHT", x = -164, y = -268, text = "Heal Buff Player" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
	},
	["WARLOCK"] = {
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 200, y = -55, text = "Buffs / Debuffs" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerTargetCCDebuff", w = 160, h = 20, anchor = "TOPLEFT", x = 37, y = -110, text = "Target CC Debuff" },
		{ name = "iFilgerTargetDoTDebuff", w = 160, h = 20, anchor = "TOPRIGHT", x = -100, y = -110, text = "Target DoT Debuff" },
		{ name = "iFilgerProcs", w = 90, h = 20, anchor = "CENTER", x = 0, y = -55, text = "Procs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "TOPRIGHT", x = -42, y = -150, text = "Cooldowns" },
		{ name = "iFilgerPetCooldowns", w = 160, h = 20, anchor = "TOPLEFT", x = 42, y = -150, text = "Pet Cooldowns" },
	},
	["DRUID"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
		{ name = "iFilgerTargetDebuff", w = 200, h = 20, anchor = "TOPRIGHT", x = 118, y = -160, text = "Target Debuff" },
		{ name = "iFilgerBuffTargetHeal", w = 200, h = 20, anchor = "TOPLEFT", x = 164, y = -268, text = "Heal Buff Target" },
		{ name = "iFilgerBuffPlayerHeal", w = 200, h = 20, anchor = "TOPRIGHT", x = -164, y = -268, text = "Heal Buff Player" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerRage", w = 165, h = 20, anchor = "CENTER", x = 0, y = -20, text = "Rage Buffs" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
	},
	["HUNTER"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerPlayerBuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Player Buffs" },
		{ name = "iFilgerTargetDebuff", w = 200, h = 20, anchor = "TOP", x = 0, y = -160, text = "Target Debuff" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerRage", w = 165, h = 20, anchor = "CENTER", x = 0, y = -20, text = "Rage Buffs" },
	},
	["ROGUE"] = {
		{ name = "iFilgerTargetDebuff", w = 160, h = 20, anchor = "TOPLEFT", x = 37, y = -110, text = "Target Debuff" },
		{ name = "iFilgerPlayerBuff", w = 160, h = 20, anchor = "TOPRIGHT", x = -42, y = -110, text = "Player Buff" },
		{ name = "iFilgerProcs", w = 90, h = 20, anchor = "CENTER", x = 0, y = -55, text = "Procs" }, -- Procs LEGENDARY
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "TOPLEFT", x = 204, y = 63, text = "Target Cooldowns" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerRage", w = 165, h = 20, anchor = "CENTER", x = 0, y = -20, text = "Rage Buffs" },
	},
	["PALADIN"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
		{ name = "iFilgerBuffTargetHeal", w = 200, h = 20, anchor = "TOPLEFT", x = 164, y = -268, text = "Heal Buff Target" },
		{ name = "iFilgerBuffPlayerHeal", w = 200, h = 20, anchor = "TOPRIGHT", x = -164, y = -268, text = "Heal Buff Player" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerPalyInvertAura", w = 200, h = 20, anchor = "BOTTOM", x = 0, y = 30, text = "Invert Auras/CD" },
	},
	["SHAMAN"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerBuffPlayerHeal", w = 200, h = 20, anchor = "TOPRIGHT", x = -164, y = -268, text = "Heal Buff Player" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
		{ name = "iFilgerBuffTargetHeal", w = 200, h = 20, anchor = "TOPLEFT", x = 164, y = -268, text = "Heal Buff Target" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerTargetDebuff", w = 160, h = 20, anchor = "TOPLEFT", x = 37, y = -110, text = "Target Debuff" },
		{ name = "iFilgerPlayerBuff", w = 160, h = 20, anchor = "TOPRIGHT", x = -42, y = -110, text = "Player Buff" },
		{ name = "iFilgerLavaBurst", w = 90, h = 20, anchor = "CENTER", x = 0, y = -55, text = "Lava Burst" },
	},
	["WARRIOR"] = {
		{ name = "iFilgerProcs", w = 200, h = 21, anchor = "BOTTOMLEFT", x = 195, y = -35, text = "Procs" },
		{ name = "iFilgerBuffDebuff", w = 200, h = 21, anchor = "TOPLEFT", x = 156, y = -141, text = "Buffs / Debuffs" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
	},
	["MONK"] = {
		{ name = "iFilgerPlayerBuff", w = 160, h = 20, anchor = "TOPLEFT", x = -42, y = -110, text = "Player Buffs" },
		{ name = "iFilgerTargetDebuff", w = 160, h = 20, anchor = "TOPLEFT", x = 37, y = -110, text = "Target Debuff" },
		{ name = "iFilgerFocusBuffs", w = 225, h = 20, anchor = "TOPRIGHT", x = -53, y = 53, text = "Focus Buffs / Debuffs" },
		{ name = "iFilgerCooldowns", w = 160, h = 20, anchor = "BOTTOMRIGHT", x = 153, y = -97, text = "Cooldowns" },
	},
	["ALL"] = {
		{ name = "iFilgerEnhancements", w = 200, h = 20, anchor = "BOTTOMRIGHT", x = 165, y = 248, text = "Enhancements" },
		{ name = "iFilgerPveDeBuffs", w = 200, h = 20, anchor = "TOPRIGHT", x = -82, y = -68, text = "PvE Debuffs" },
		{ name = "iFilgerPvpPlayerDebuffs", w = 250, h = 20, anchor = "BOTTOMRIGHT", x = -104, y = 93, text = "PvP Player Debuffs" },
		{ name = "iFilgerPvpTargetBuffs", w = 250, h = 20, anchor = "BOTTOMLEFT", x = 104, y = 93, text = "PvP Target Buffs" },
		{ name = "iFilgerTanksCDs", w = 200, h = 20, anchor = "BOTTOM", x = 0, y = 100, text = "Tanks CDs" },
		{ name = "iFilgerTanksCDsFocus", w = 200, h = 20, anchor = "TOPRIGHT", x = -220, y = 53, text = "Tanks focus CDs" },
	},
}

--	List of available options:
--		spellID	
--			= any number up to 132122 or so
--			(every spell has its unique spellID)
--			(you can find out spellID by looking up the spell on wowhead and checking URL of the page - http://www.wowhead.com/spell=5782 - in this case spellID = 5782)
--		unitId
--			= "player" / "target" / "focus" / "pet" / "party1" / "arena1" / "targettarget" / "targettargettarget" / "party1target" / "pettarget" / "focustarget" 
--			(note that you cannot use "targetfocus" and other similar options as you can only see your own focus)
--		caster
--			= "all" / "player" / "target" / "pet"
--			(Whoever casted the spell. If set to all, anyone's spell get displayed)
--		filter
--			= "BUFF" / "DEBUFF" / "IBUFF" / "IDEBUFF" / "CD" / "ACD" / "ICD"
--			(BUFF checks if buff is applied)
--			(DEBUFF checks if debuff is applied)
--			(IBUFF checks if buff is NOT applied)
--			(IDEBUFF checks if debuff is NOT applied)
--			(CD checks if spell is on cooldown)
--			(ACD checks if spell is NOT on cooldown)
--			(ICD checks for internal cooldown of the spell)
--		trigger
--			= "BUFF" / "DEBUFF"
--			(works only with filter option set to ICD)
--			(specifies if triggering spell applies buff or debuff)
--		duration
--			= any number (in seconds)
--			(works only with filter option set to ICD)
--			(specifies how long internal CD is)
--		timeleft
--			= any number (in seconds)
--			(works only with filter option set to buff or debuff)
--			(show icon only if time left is below)
--		slotID
--			= any number lower than 24
--			(works only with filter option set to CD / ICD)
--			(if slotID is specified, icon graphics will change to icon of item equipped in slot XX)
--			(1 = head, 2 = neck, 3 = shoulder, 4 = shirt, 5 = chest, 6 = belt, 7 = legs, 8 = feet, 9 = wrist, 10 = gloves, 
--			11 = finger 1, 12 = finger 2, 13 = trinket 1, 14 = trinket 2, 15 = back, 16 = main hand, 17 = off hand, 18 = ranged?, 19 = tabard, 
--			20 = first bag (the rightmost one), 21 = second bag, 22 = third bag, 23 = fourth bag (the leftmost one)
--		incombat
--			= true
--			(if incombat option is set to true, icon or bar will only show if in combat)
--		spec
--			= 1 / 2 / 3 / 4
--			(icon or bar will only show if spec option is not specified or spec option matches your specialization number)
--		absID
--			(if set to true, spell will get displayed only if spell ID matches with spellID option, otherwise it will display if names match)
--		icon
--			= [[Interface\Icons\Spell_Frost_Frost]] / [[Interface\Icons\spell_mage_infernoblast]]
--			(changes icon graphics to a texture path specified in this option)
--			(specifying icon option, slotID option will cease to function)

C["Filger_Spells"] = {
	["DEATHKNIGHT"] = {
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
 			{ spellID = 56222, filter = "CD" }, -- Dark Command
			{ spellID = 48743, filter = "CD" }, -- Death Pact
			{ spellID = 47528, filter = "CD" }, -- Mind Freeze
			{ spellID = 48792, filter = "CD" }, -- Icebound Fortitude
			{ spellID = 47568, filter = "CD" }, -- Empower Rune Weapon
			{ spellID = 49184, filter = "CD" }, -- Howling Blast
			{ spellID = 49576, filter = "CD" }, -- Death Grip
			{ spellID = 46584, filter = "CD" }, -- Raise Dead
			{ spellID = 48707, filter = "CD" }, -- Anti-Magic Shell
			{ spellID = 43265, filter = "CD" }, -- Death and Decay
			{ spellID = 55233, filter = "CD" }, -- Vampiric Blood
			{ spellID = 49028, filter = "CD" }, -- Dancing Rune Weapon
			{ spellID = 49039, filter = "CD" }, -- Lichborne
			{ spellID = 51052, filter = "CD" }, -- Anti-Magic Zone
			{ spellID = 49206, filter = "CD" }, -- Summon Gargoyle
			{ spellID = 77575, filter = "CD" }, -- Outbreak
			{ spellID = 77606, filter = "CD" }, -- Dark Simulacrum
		},
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 91342, unitId = "pet", caster = "all", filter = "BUFF" },			-- Shadow Infusion
			{ spellID = 63560, unitId = "pet", caster = "all", filter = "BUFF" }, 			-- Dark Transformation
			{ spellID = 73975, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Necrotic Strike
			{ spellID = 59052, unitId = "player", caster = "all", filter = "BUFF" },		-- Freezing Fog
			{ spellID = 49509, unitId = "player", caster = "all", filter = "BUFF" },		-- Scent of Blood
			{ spellID = 55233, unitId = "player", caster = "all", filter = "BUFF" },		-- Vampiric Blood
			{ spellID = 49039, unitId = "player", caster = "all", filter = "BUFF" },		-- Lichborne
			{ spellID = 3714, unitId = "player", caster = "all", filter = "BUFF" },			-- Path of Frost
			{ spellID = 47476, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Strangulate
			{ spellID = 45524, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Chains of Ice
			{ spellID = 59921, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Frost Fever
			{ spellID = 43265, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Death and Decay
			{ spellID = 59879, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Blood Plague
			{ spellID = 115989, unitId = "player", caster = "player", filter = "BUFF" },	-- Unholy Blight
			{ spellID = 77535, unitId = "player", caster = "player", filter = "BUFF" },		-- Blood Shield
			{ spellID = 119975, unitId = "player", caster = "player", filter = "BUFF" },	-- Conversion
			{ spellID = 96268, unitId = "player", caster = "player", filter = "BUFF" },		-- Death's Advance
		},
		{
			Name = "DK Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 49530, unitId = "player", caster = "all", filter = "BUFF" },		-- Sudden Doom
			{ spellID = 81141, unitId = "player", caster = "all", filter = "BUFF" },		-- Crimson Scourge
			{ spellID = 51128, unitId = "player", caster = "all", filter = "BUFF" },		-- Killing Machine
			{ spellID = 101568, unitId = "player", caster = "player", filter = "BUFF" },	-- Dark Succor
			{ spellID = 59052, unitId = "player", caster = "all", filter = "BUFF" },		-- Freezing Fog
		},
	},
	["MAGE"] = {
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37, 
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 33395, filter = "CD" },		-- Water Elemental Freeze
			{ spellID = 11113, filter = "CD" },		-- Blast Wave
			{ spellID = 2136, filter = "CD" },		-- Fire Blast
			{ spellID = 2139, filter = "CD" },		-- Counterspell
			{ spellID = 44572, filter = "CD" }, 	-- Deep Freeze
			{ spellID = 12042, filter = "CD" },		-- Arcane Power
			{ spellID = 12472, filter = "CD" },		-- Icy Veins
			{ spellID = 11129, filter = "CD" },		-- Combustion
			{ spellID = 110909, filter = "CD" },	-- Alter Time
			{ spellID = 12043, filter = "CD" },		-- Presence of Mind
			{ spellID = 1463, filter = "CD" },		-- Incanter's Ward
			{ spellID = 87024, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Cauterize
		},
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 116, unitId = "target", caster = "player", filter = "DEBUFF" },		-- Frostbolt
			{ spellID = 31589, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Slow
			{ spellID = 12472, unitId = "player", caster = "player", filter = "BUFF" },		-- Icy Veins
			{ spellID = 12042, unitId = "player", caster = "all", filter = "BUFF" },		-- Arcane Power
			{ spellID = 45438, unitId = "player", caster = "player", filter = "BUFF" },		-- Ice Block
			{ spellID = 66, unitId = "player", caster = "all", filter = "BUFF" },			-- Invisibility
			{ spellID = 132210, unitId = "player", caster = "all", filter = "BUFF" },		-- Pyromaniac (fire damage increased by 10%, applyied by bomb)
			{ spellID = 11366, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Pyroblast
			{ spellID = 44457, unitId = "target", caster = "player", filter = "DEBUFF" }, 	-- Living Bomb
			{ spellID = 112948, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Frost Bomb
			{ spellID = 114923, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Nether Tempest
			{ spellID = 12846, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Ignite
			{ spellID = 11129, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Combustion
			{ spellID = 44572, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Deep Freeze
			{ spellID = 33395, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Freeze
			{ spellID = 122, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Frost Nova
			{ spellID = 82691, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Ring of Frost
			{ spellID = 102051, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Frostjaw
			{ spellID = 46989, unitId = "player", caster = "player", filter = "BUFF" },		-- Rapid teleportation
			{ spellID = 110960, unitId = "player", caster = "player", filter = "BUFF" },	-- Greater Invisibility
			{ spellID = 5405, unitId = "player", caster = "player", filter = "BUFF" },		-- Replendish Mana
			{ spellID = 116014, unitId = "player", caster = "player", filter = "BUFF" },	-- Rune of Power
			{ spellID = 1463, unitId = "player", caster = "player", filter = "BUFF" },		-- Incanter's Ward
			{ spellID = 116257, unitId = "player", caster = "player", filter = "BUFF" },	-- Invoker's Energy
		},
		{
			Name = "Mage Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 79683, unitId = "player", caster = "player", filter = "BUFF" },					-- Missiles
			{ spellID = 57761, unitId = "player", caster = "player", filter = "BUFF" },					-- Brain Freeze
			{ spellID = 48107, unitId = "player", caster = "player", filter = "BUFF", icon = [[Interface\Icons\spell_mage_infernoblast]] },	-- Heating Up 
			{ spellID = 48108, unitId = "player", caster = "player", filter = "BUFF" },					-- Hot Streak
			{ spellID = 36032, unitId = "player", caster = "player", filter = "DEBUFF" },				-- Arcane Charge
			{ spellID = 112965, unitId = "player", caster = "player", filter = "BUFF"},					-- Fingers Of Frost
			{ spellID = 108843, unitId = "player", caster = "all", filter = "BUFF" },					-- Blazing Speed
			{ spellID = 87023, unitId = "player", caster = "player", filter = "DEBUFF", absID = true },	-- Cauterize
			{ spellID = 110909, unitId = "player", caster = "player", filter = "BUFF" },				-- Alter Time
			{ spellID = 115610, unitId = "player", caster = "player", filter = "DEBUFF" },				-- Temporal Shield	
			{ spellID = 108839, unitId = "player", caster = "player", filter = "BUFF" },				-- Ice Floes
			{ spellID = 115701, unitId = "player", caster = "player", filter = "BUFF" },				-- Glyph of Remove Curse
			{ spellID = 116257, unitId = "player", caster = "player", filter = "BUFF", timeleft = 5 },	-- Invoker's Energy
			{ spellID = 116267, unitId = "player", caster = "player", filter = "BUFF" },				-- Invoker's Absorption
		},
		{
			Name = "Mage InvertAura",
			Enable = true,
			Direction = "HORIZONTAL",
			Interval = 4,
			Mode = "ICON",
			Alpha = 0.75,
--			BarWidth = 150,
			Size = 47,
			setPoint = { "BOTTOM", "iFilgerMageInvertAura", 0, 22 },
			{ spellID = 116014, unitId = "player", caster = "player", filter = "IBUFF", known = 116011 },				-- Rune of Power
			{ spellID = 116257, unitId = "player", caster = "player", filter = "IBUFF", known = 114003 },				-- Invoker's Energy
			{ spellID = 132210, unitId = "target", caster = "player", filter = "IDEBUFF", incombat = true, spec = 2 },	-- Pyromaniac (fire damage increased by 10%, applyied by bomb)
			{ spellID = 112948, unitId = "target", caster = "player", filter = "IDEBUFF", incombat = true, spec = 3 },	-- Frost Bomb
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
			Interval = 2,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 36,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 118, unitId = "focus", caster = "player", filter = "DEBUFF" }, -- Polymorph
		},
		{
			Name = "Interrupt",
			Enable = true,
			Direction = "HORIZONTAL",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 36,
			setPoint = { "TOP", "iFilgerInterrupt", 0, -22 },
			{ spellID = 116, filter = "CD" },	-- Frostbolt
			{ spellID = 133, filter = "CD" },	-- Fireball
			{ spellID = 118, filter = "CD" },	-- Polymorph
		},
	},
	["PRIEST"] = {
		{
			Name = "Self Buffs",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 32,
			setPoint = { "BOTTOMRIGHT", "iFilgerBuffPlayerHeal", 0, 24 },
			{ spellID = 588, unitId = "player", caster = "player", filter = "BUFF" },	-- Inner Fire
			{ spellID = 73413, unitId = "player", caster = "player", filter = "BUFF" },	-- Inner Will
			{ spellID = 6346, unitId = "player", caster = "player", filter = "BUFF" },	-- Fear Ward
		},
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 114214, filter = "ICD", trigger = "BUFF", duration = 90 },	-- Angelic Bulwark
			{ spellID = 81700, filter = "CD" },		-- Archangel
			{ spellID = 121135, filter = "CD" },	-- Cascade
			{ spellID = 34861, filter = "CD" },		-- Circle of Healing
			{ spellID = 19236, filter = "CD" },		-- Desperate Prayer
			{ spellID = 47585, filter = "CD" }, 	-- Dispersion
			{ spellID = 64843, filter = "CD" },		-- Divine Hymn
			{ spellID = 110744, filter = "CD" },	-- Divine Star
			{ spellID = 605, filter = "CD" },		-- Dominate Mind
			{ spellID = 586, filter = "CD" },		-- Fade
			{ spellID = 6346, filter = "CD" },		-- Fear Ward
			{ spellID = 47788, filter = "CD" },		-- Guardian Spirit
			{ spellID = 120517, filter = "CD" },	-- Halo
			{ spellID = 88625, filter = "CD" },		-- Holy Word: Chastise
			{ spellID = 64901, filter = "CD" },		-- Hymn of Hope
			{ spellID = 89485, filter = "CD" }, 	-- Inner Focus
			{ spellID = 73325, filter = "CD" },		-- Leap of Faith
			{ spellID = 724, filter = "CD" },		-- Lightwell
			{ spellID = 32375, filter = "CD" },		-- Mass Dispel
			{ spellID = 47540, filter = "CD" },		-- Penance
			{ spellID = 10060, filter = "CD" },		-- Power Infusion
			{ spellID = 62618, filter = "CD" },		-- Power Word: Barrier
			{ spellID = 33076, filter = "CD" },		-- Prayer of Mending
			{ spellID = 64044, filter = "CD" },		-- Psychic Horror
			{ spellID = 8122, filter = "CD" },		-- Psychic Scream
			{ spellID = 527, filter = "CD" },		-- Purify
			{ spellID = 34433, filter = "CD" },		-- Shadowfiend
			{ spellID = 15487, filter = "CD" },		-- Silence
			{ spellID = 112833, filter = "CD" },	-- Spectral Guise
			{ spellID = 109964, filter = "CD" },	-- Spirit Shell
			{ spellID = 15286, filter = "CD" },		-- Vampiric Embrace
			{ spellID = 108920, filter = "CD" },	-- Void Tendrils
			{ spellID = 108968, filter = "CD" },	-- Void Shift
		},
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 81208, unitId = "player", caster = "player", filter = "BUFF" },					-- Chakra: Serenity
			{ spellID = 81206, unitId = "player", caster = "player", filter = "BUFF" },					-- Chakra: Sanctuary
			{ spellID = 81209, unitId = "player", caster = "player", filter = "BUFF" },					-- Chakra: Chastise
			{ spellID = 114239, unitId = "player", caster = "player", filter = "BUFF" },				-- Phantasm
			{ spellID = 119032, unitId = "player", caster = "player", filter = "BUFF" },				-- Spectral Guise
			{ spellID = 47788, unitId = "player", caster = "player", filter = "BUFF" },					-- Guardian Spirit
			{ spellID = 33206, unitId = "player", caster = "player", filter = "BUFF" },					-- Pain Suppression
			{ spellID = 139, unitId = "player", caster = "player", filter = "BUFF" },					-- Renew
			{ spellID = 17, unitId = "player", caster = "player", filter = "BUFF" },					-- Power Word: Shield
			{ spellID = 89485, unitId = "player", caster = "player", filter = "BUFF" },					-- Inner Focus
			{ spellID = 586, unitId = "player", caster = "player", filter = "BUFF" },					-- Fade
			{ spellID = 59889, unitId = "player", caster = "player", filter = "BUFF" },					-- Borrowed Time
			{ spellID = 109964, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Spirit Shell
			{ spellID = 105826, unitId = "player", caster = "player", filter = "BUFF" },				-- Temporal Boon - 2T13 Heal
		},
		{
			Name = "Priest Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 6788, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Weakened Soul
			{ spellID = 96267, unitId = "player", caster = "player", filter = "BUFF" },					-- Glyph of Inner Focus
			{ spellID = 27827, unitId = "player", caster = "all", filter = "BUFF" },					-- Spirit of Redemption
			{ spellID = 63735, unitId = "player", caster = "player", filter = "BUFF" },					-- Serendipity
			{ spellID = 81700, unitId = "player", caster = "player", filter = "BUFF" },					-- Archangel
			{ spellID = 81662, unitId = "player", caster = "player", filter = "BUFF" },					-- Evangelism
			{ spellID = 114255, unitId = "player", caster = "all", filter = "BUFF" },					-- Surge of Light
			{ spellID = 123266, unitId = "player", caster = "all", filter = "BUFF", absID = true },		-- Divine Insight : Shield
			{ spellID = 123267, unitId = "player", caster = "all", filter = "BUFF", absID = true },		-- Divine Insight : PoM
			{ spellID = 123254, unitId = "player", caster = "player", filter = "BUFF", spec = 1 },		-- Twist of Fate
			{ spellID = 123254, unitId = "player", caster = "player", filter = "BUFF", spec = 2 },		-- Twist of Fate
			{ spellID = 114214, unitId = "player", caster = "player", filter = "BUFF" },				-- Angelic Bulwark
		},	
		{
			Name = "Buffs and Debuffs HEAL",
			Enable = true,
			Direction = "LEFT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOPRIGHT", "iFilgerBuffDebuffHeal", 20, -22 },
			{ spellID = 41635, unitId = "target", caster = "player", filter = "BUFF" },	-- Prayer of Mending
			{ spellID = 47788, unitId = "target", caster = "player", filter = "BUFF" },	-- Guardian Spirit
			{ spellID = 33206, unitId = "target", caster = "player", filter = "BUFF" },	-- Pain Suppression
			{ spellID = 17, unitId = "target", caster = "player", filter = "BUFF" },	-- Power Word: Shield
			{ spellID = 139, unitId = "target", caster = "player", filter = "BUFF" },	-- Renew
			{ spellID = 6346, unitId = "target", caster = "player", filter = "BUFF" },	-- Fear Ward
			{ spellID = 77489, unitId = "target", caster = "player", filter = "BUFF" },	-- Echo of Light
			{ spellID = 77613, unitId = "target", caster = "player", filter = "BUFF" },	-- Grace
			{ spellID = 6788, unitId = "target", caster = "all", filter = "BUFF" },		-- Weakened Soul
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
			IconSide = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 32,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 9484, unitId = "focus", caster = "all", filter = "DEBUFF" },		-- Shackle Undead
			{ spellID = 8122, unitId = "focus", caster = "all", filter = "DEBUFF" },		-- Psychic Scream
			{ spellID = 589, unitId = "focus", caster = "player", filter = "DEBUFF" },		-- Shadow Word: Pain
			{ spellID = 2944, unitId = "focus", caster = "player", filter = "DEBUFF" },		-- Devouring Plague
			{ spellID = 34914, unitId = "focus", caster = "player", filter = "DEBUFF" },	-- Vampiric Touch
		},
		{	
			Name = "Cooldown DPS",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 36,
			setPoint = { "BOTTOM", "iFilgerProcDps", 0, 52 },
			{ spellID = 8092, filter = "CD" },	-- Mind Blast
			{ spellID = 14914, filter = "CD" },	-- Holy Fire
		},
		{
			Name = "Buffs DPS",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 36,
			setPoint = { "BOTTOMRIGHT", "iFilgerBuffDps", 0, 22 },
			{ spellID = 47585, unitId = "player", caster = "player", filter = "BUFF" },					-- Dispersion
			{ spellID = 87160, unitId = "player", caster = "all", filter = "BUFF" },					-- Surge of Darkness
			{ spellID = 124430, unitId = "player", caster = "all", filter = "BUFF", absID = true },		-- Divine Insight - Shadow
			{ spellID = 81292, unitId = "player", caster = "all", filter = "BUFF" },					-- Glyph of Mind Spike
			{ spellID = 123254, unitId = "player", caster = "player", filter = "BUFF", spec = 3},		-- Twist of Fate
		},
		{
			Name = "Debuffs DPS",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 36,
			setPoint = { "BOTTOMLEFT", "iFilgerDebuffDps", 0, 22},
			{ spellID = 9484, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Shackle undead
			{ spellID = 8122, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Psychic Scream
			{ spellID = 589, unitId = "target", caster = "player", filter = "DEBUFF" },		-- Shadow Word: Pain
			{ spellID = 2944, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Devouring Plague
			{ spellID = 34914, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Vampiric Touch
		},
	},
	["WARLOCK"] = {
		{
			Name = "Warlock Procs",
			Enable = true,
			Direction = "DOWN",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOP", "iFilgerProcs", -0, -23 },
			{ spellID = 34936, unitId = "player", caster = "player", filter = "BUFF" },		-- Backlash
			{ spellID = 99232, unitId = "player", caster = "player", filter = "BUFF" },		-- Apocalypse T12 4P
			{ spellID = 122351, unitId = "player", caster = "player", filter = "BUFF" },	-- Molten Core
			{ spellID = 108869, unitId = "player", caster = "player", filter = "BUFF" },	-- Decimation
			{ spellID = 117896, unitId = "player", caster = "player", filter = "BUFF" },	-- Backdraft
			{ spellID = 88448, unitId = "player", caster = "player", filter = "BUFF" },		-- Demonic Rebirth
			{ spellID = 108558, unitId = "player", caster = "player", filter = "BUFF" },	-- Nightfall
			{ spellID = 74434, unitId = "player", caster = "player", filter = "BUFF" },		-- Soulburn
		},
		{
			Name = "Buffs and Debuffs DPS",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerTargetCCDebuff", 0, 22},
			{ spellID = 80240, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Havoc
			{ spellID = 980, unitId = "target", caster = "player", filter = "DEBUFF" },		-- Agony
			{ spellID = 603, unitId = "target", caster = "player", filter = "DEBUFF" },		-- Doom
			{ spellID = 30108, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Unstable Affliction
			{ spellID = 172, unitId = "target", caster = "player", filter = "DEBUFF" },		-- Corruption
			{ spellID = 348, unitId = "target", caster = "player", filter = "DEBUFF" },		-- Immolate
			{ spellID = 48181, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Haunt
			{ spellID = 27243, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Seed of Corruption
		},
		{
			Name = "Buffs and Debuffs CC",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerTargetDoTDebuff", 58, 22},
			{ spellID = 47960, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Shadowflame
			{ spellID = 89751, unitId = "pet", caster = "all", filter = "BUFF" },			-- Felstorm
			{ spellID = 1490, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Curse of the Elements
			{ spellID = 109466, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Curse of Emfeeblement
			{ spellID = 18223, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Curse of Exhaustion
			{ spellID = 5782, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Fear
			{ spellID = 111397, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Blood Fear
			{ spellID = 710, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Banish
			{ spellID = 5484, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Howl of Terror
			{ spellID = 6789, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Mortal Coil
			{ spellID = 1098, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Enslave Demon
			{ spellID = 54785, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Demon Charge
			{ spellID = 109773, unitId = "target", caster = "player", filter = "BUFF" },	-- Dark Intent
			{ spellID = 20707, unitId = "target", caster = "player", filter = "BUFF" },		-- Soulstone Resurrection
			{ spellID = 105174, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Hand of Gul'dan
		},
		{
			Name = "Player Buffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 113860, unitId = "player", caster = "player", filter = "BUFF" },	-- Dark Soul: Misery
			{ spellID = 113858, unitId = "player", caster = "player", filter = "BUFF" },	-- Dark Soul: Instability
			{ spellID = 113861, unitId = "player", caster = "player", filter = "BUFF" },	-- Dark Soul: Knowledge
			{ spellID = 86121, unitId = "player", caster = "player", filter = "BUFF" },		-- Soul Swap
			{ spellID = 6229, unitId = "player", caster = "player", filter = "BUFF" },		-- Twilight Ward
			{ spellID = 104773, unitId = "player", caster = "player", filter = "BUFF" },	-- Unending Resolve
			{ spellID = 111400, unitId = "player", caster = "player", filter = "BUFF" },	-- Burning Rush
			{ spellID = 108359, unitId = "player", caster = "player", filter = "BUFF" },	-- Dark Regeneration
			{ spellID = 110913, unitId = "player", caster = "player", filter = "BUFF" },	-- Dark Bargain
			{ spellID = 108416, unitId = "player", caster = "player", filter = "BUFF" },	-- Sacrificial Pact
			{ spellID = 108415, unitId = "player", caster = "player", filter = "BUFF" },	-- Soul Link
		},
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 33,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 0, 22 },
			{ spellID = 119898, filter = "CD" },	-- Command Demon
			{ spellID = 108505, filter = "CD" },	-- Archimonde's Vengeance
			{ spellID = 108501, filter = "CD" },	-- Grimoire of Service
			{ spellID = 108482, filter = "CD" },	-- Unbound Will
			{ spellID = 111397, filter = "CD" },	-- Blood Fear
			{ spellID = 108416, filter = "CD" },	-- Sacrificial Pact
			{ spellID = 110913, filter = "CD" },	-- Dark Bargain
			{ spellID = 108359, filter = "CD" },	-- Dark Regeneration
			{ spellID = 77801, filter = "CD" },		-- Demon Soul
			{ spellID = 1122, filter = "CD" },		-- Infernal
			{ spellID = 17962, filter = "CD" },		-- Conflagrate
			{ spellID = 6229, filter = "CD" },		-- Shadow Ward
			{ spellID = 54785, filter = "CD" },		-- Demon Leap
			{ spellID = 29858, filter = "CD" },		-- Soulshatter
			{ spellID = 48020, filter = "CD" },		-- Demonic Circle: Teleport
			{ spellID = 5484, filter = "CD" },		-- Howl of Terror
			{ spellID = 6789, filter = "CD" },		-- Mortal Coil
			{ spellID = 48181, filter = "CD" },		-- Haunt
			{ spellID = 17877, filter = "CD" },		-- Shadowburn
			{ spellID = 30283, filter = "CD" },		-- Shadowfury
			{ spellID = 116858, filter = "CD" },	-- Chaos Bolt
			{ spellID = 101976, filter = "CD" },	-- Soul Harvest
			{ spellID = 89751, filter = "CD" },		-- Felstorm
			{ spellID = 74434, filter = "CD" },		-- Soulburn
			{ spellID = 105174, filter = "CD" },	-- Hand of Gul'dan
			{ spellID = 80240, filter = "CD" },		-- Havoc
			{ spellID = 120451, filter = "CD" },	-- Flames of Xoroth
			{ spellID = 103967, filter = "CD" },	-- Carrion Swarm
		},
		{
			Name = "Pet Cooldown",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 33,
			setPoint = { "BOTTOMLEFT", "iFilgerPetCooldowns", 0, 22 },
			{ spellID = 115831, filter = "CD" },	-- Wrathstorm 				{ spellID = 119915, filter = "CD" }
			{ spellID = 89766, filter = "CD" },		-- Axe Toss
			{ spellID = 115276, filter = "CD" },	-- Sear Magic
			{ spellID = 119899, filter = "CD" },	-- Cauterize Master 		{ spellID = 119905, filter = "CD" }
			{ spellID = 119911, filter = "CD" },	-- Optical Blast			{ spellID = 115781, filter = "CD" },
			{ spellID = 115284, filter = "CD" },	-- Clone Magic
			{ spellID = 115268, filter = "CD" },	-- Mesmerize
			{ spellID = 119913, filter = "CD" },	-- Fellash					{ spellID = 115770, filter = "CD" }
			{ spellID = 19505, filter = "CD" },		-- Devour Magic (Felhunter)
			{ spellID = 19647 , filter = "CD" },	-- Spell Lock (Felhunter)	{ spellID = 119910 , filter = "CD" } \ { spellID = 132409 , filter = "CD" }
			{ spellID = 6360 , filter = "CD" },		-- Whiplash					{ spellID = 119909 , filter = "CD" }
			{ spellID = 6358 , filter = "CD" },		-- Seduction				{ spellID = 132412 , filter = "CD" }
			{ spellID = 89808 , filter = "CD" },	-- Singe Magic				{ spellID = 132411 , filter = "CD" }
			{ spellID = 17735, filter = "CD" },		-- Suffering (Voidwalker)
			{ spellID = 132413 , filter = "CD" },	-- Shadow Bulwark (Voidwalker)
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 80240, unitId = "focus", caster = "player", filter = "DEBUFF" },	-- Havoc
			{ spellID = 5782, unitId = "focus", caster = "all", filter = "DEBUFF" },		-- Fear
			{ spellID = 710, unitId = "focus", caster = "all", filter = "DEBUFF" },			-- Banish
		},
	},
	["DRUID"] = {
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 32,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 102280, filter = "CD", absID = true },	-- Displacer Beast
			{ spellID = 102401, filter = "CD", absID = true },	-- Wild Charge
			{ spellID = 78674, filter = "CD" },					-- Starsurge
			{ spellID = 48505, filter = "CD" },					-- Starfall
			{ spellID = 22842, filter = "CD" },					-- Frenzied Regeneration
			{ spellID = 88423, filter = "CD" },					-- Nature Cure
		},
		{
			Name = "Buffs Player Heal",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 32,
			setPoint = { "BOTTOMRIGHT", "iFilgerBuffPlayerHeal", 0, 24 },
			{ spellID = 33763, unitId = "player", caster = "player", filter = "BUFF" },	-- Lifebloom
			{ spellID = 774, unitId = "player", caster = "player", filter = "BUFF" },	-- Rejuvenation
			{ spellID = 8936, unitId = "player", caster = "player", filter = "BUFF" },	-- Regrowth
			{ spellID = 48438, unitId = "player", caster = "player", filter = "BUFF" },	-- Wild Growth
		},
		{
			Name = "Buffs Target Heal",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 32,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffTargetHeal", 0, 24 },
			{ spellID = 33763, unitId = "target", caster = "player", filter = "BUFF" },	-- Lifebloom
			{ spellID = 774, unitId = "target", caster = "player", filter = "BUFF" },	-- Rejuvenation
			{ spellID = 8936, unitId = "target", caster = "player", filter = "BUFF" },	-- Regrowth
			{ spellID = 48438, unitId = "target", caster = "player", filter = "BUFF" },	-- Wild Growth
		},
		{
			Name = "Buffs player",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 117679, unitId = "player", caster = "player", filter = "BUFF" },	-- Incarnation: Tree of Life
			{ spellID = 102543, unitId = "player", caster = "player", filter = "BUFF" },	-- Incarnation: King of the Jungle
			{ spellID = 102558, unitId = "player", caster = "player", filter = "BUFF" },	-- Incarnation: Son of Ursoc
			{ spellID = 102560, unitId = "player", caster = "player", filter = "BUFF" },	-- Incarnation: Chosen of Elune
			{ spellID = 112071, unitId = "player", caster = "player", filter = "BUFF" },	-- Celestial Alignment
			{ spellID = 52610, unitId = "player", caster = "player", filter = "BUFF" },		-- Savage Roar
			{ spellID = 124974, unitId = "player", caster = "player", filter = "BUFF" },	-- Nature's Vigil
			{ spellID = 16689, unitId = "player", caster = "player", filter = "BUFF" },		-- Nature's Grasp
			{ spellID = 1850, unitId = "player", caster = "player", filter = "BUFF" },		-- Dash
		},
		{
			Name = "Druid Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 16886, unitId = "player", caster = "player", filter = "BUFF" },					-- Nature's Grace
			{ spellID = 100977, unitId = "player", caster = "player", filter = "BUFF" },				-- Harmony (Healer Mastery Buff)
			{ spellID = 48518, unitId = "player", caster = "player", filter = "BUFF" },					-- Eclipse (Lunar)
			{ spellID = 48517, unitId = "player", caster = "player", filter = "BUFF" },					-- Eclipse (Solar)
			{ spellID = 93400, unitId = "player", caster = "player", filter = "BUFF" },					-- Shooting Stars
			{ spellID = 48391, unitId = "player", caster = "player", filter = "BUFF" },					-- Owlkin Frenzy
			{ spellID = 81192, unitId = "player", caster = "player", filter = "BUFF" },					-- Lunar Shower
			{ spellID = 48505, unitId = "player", caster = "player", filter = "BUFF" },					-- Starfall
			{ spellID = 61336, unitId = "player", caster = "player", filter = "BUFF" },					-- Survival Instincts
			{ spellID = 102342, unitId = "player", caster = "player", filter = "BUFF" },				-- Ironbark
			{ spellID = 106922, unitId = "player", caster = "player", filter = "BUFF" },				-- Might of Ursoc
			{ spellID = 5229, unitId = "player", caster = "player", filter = "BUFF" },					-- Enrage
			{ spellID = 106898, unitId = "player", caster = "player", filter = "BUFF" },				-- Stampeding Roar
			{ spellID = 106951, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Berserk - Cat
			{ spellID = 50334, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Berserk - Bear
			{ spellID = 5217, unitId = "player", caster = "player", filter = "BUFF" },					-- Tiger's Fury
			{ spellID = 16870, unitId = "player", caster = "player", filter = "BUFF" },					-- Clearcasting
			{ spellID = 29166, unitId = "player", caster = "all", filter = "BUFF" },					-- Innervate
			{ spellID = 22812, unitId = "player", caster = "player", filter = "BUFF" },					-- Barkskin
			{ spellID = 63058, unitId = "player", caster = "player", filter = "BUFF" },					-- Glyph of Barkskin
			{ spellID = 740, unitId = "player", caster = "player", filter = "BUFF" },					-- Tranquility
			{ spellID = 105713, unitId = "player", caster = "player", filter = "BUFF" },				-- Natural Harmony - 2T13 Heal
		},
		{
			Name = "Debuffs Target",
			Enable = true,
			Direction = "LEFT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOPRIGHT", "iFilgerTargetDebuff", 0, -24 },
			{ spellID = 22570, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Maim
			{ spellID = 2637, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Hibernate
			{ spellID = 339, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Entangling Roots
			{ spellID = 33786, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Cyclone
			{ spellID = 8921, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Moonfire
			{ spellID = 93402, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Sunfire
			{ spellID = 5570, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Insect Swarm
			{ spellID = 1822, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Rake
			{ spellID = 1079, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Rip
			{ spellID = 33745, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Lacerate
			{ spellID = 9007, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Pounce Bleed
			{ spellID = 33876, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Mangle
			{ spellID = 770, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Faerie Fire
			{ spellID = 9005, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Pounce
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Alpha = 1,
			Mode = "ICON",
			Size = 32,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 2637, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Hibernate
			{ spellID = 339, unitId = "focus", caster = "all", filter = "DEBUFF" },		-- Entangling Roots
			{ spellID = 33786, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Cyclone
		},
	},
	["HUNTER"] = {
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 3045, filter = "CD" },	-- Rapid Fire
			{ spellID = 5384, filter = "CD" },	-- Feign Death
			{ spellID = 19574, filter = "CD" },	-- Bestial Wrath
		},
		{
			Name = "Hunter Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 56453, unitId = "player", caster = "player", filter = "BUFF" },	-- Lock and Load
			{ spellID = 53224, unitId = "player", caster = "player", filter = "BUFF" },	-- Improved Steady Shot
			{ spellID = 82925, unitId = "player", caster = "player", filter = "BUFF" },	-- Ready, Set, Aim
			{ spellID = 82926, unitId = "player", caster = "player", filter = "BUFF" },	-- Fire!
			{ spellID = 53257, unitId = "player", caster = "player", filter = "BUFF" },	-- Cobra Strikes
			{ spellID = 82692, unitId = "player", caster = "player", filter = "BUFF" },	-- Focus Fire
			{ spellID = 34720, unitId = "player", caster = "player", filter = "BUFF" },	-- Thrill of the Hunt
		},
		{
			Name = "Player Buffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerPlayerBuff", 0, 24 },
			{ spellID = 3045, unitId = "player", caster = "player", filter = "BUFF" },		-- Rapid Fire
			{ spellID = 136, unitId = "pet", caster = "player", filter = "BUFF" },			-- Mend Pet
			{ spellID = 90361, unitId = "player", caster = "player", filter = "BUFF" },		-- Spirit Mend
			{ spellID = 19263, unitId = "player", caster = "player", filter = "BUFF" },		-- Deterrence
			{ spellID = 64420, unitId = "player", caster = "player", filter = "BUFF" },		-- Sniper Training
			{ spellID = 34471, unitId = "player", caster = "player", filter = "BUFF" },		-- The Beast Within
			{ spellID = 51755, unitId = "player", caster = "player", filter = "BUFF" },		-- Camouflage
			{ spellID = 118922, unitId = "player", caster = "player", filter = "BUFF" },	-- Posthaste
		},
		{
			Name = "Target Debuffs",
			Enable = true,
			Direction = "HORIZONTAL",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOP", "iFilgerTargetDebuff", 0, -24 },
			{ spellID = 19386, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Wyvern Sting
			{ spellID = 34490, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Silencing Shot
			{ spellID = 1978, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Serpent Sting
			{ spellID = 82654, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Widow Venom
			{ spellID = 3674, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Black Arrow
			{ spellID = 53301, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Explosive Shot
			{ spellID = 1130, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Hunter's Mark
			{ spellID = 63468, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Piercing Shots
			{ spellID = 131894, unitId = "target", caster = "player", filter = "DEBUFF" },	-- A Murder of Crows
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
			IconSide = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
			Size = 32,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 19386, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Wyvern Sting
			{ spellID = 34490, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Silencing Shot
			{ spellID = 3355, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Freezing Trap
		},
	},	
	["ROGUE"] = {
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOPLEFT", "iFilgerCooldowns", 0, -22 },
			{ spellID = 2983, filter = "CD" },										-- Sprint
			{ spellID = 31224, filter = "CD" },										-- Cloak of Shadows
			{ spellID = 57934, filter = "CD" },										-- Tricks of the Trade
			{ spellID = 408, filter = "CD" },										-- Kidney Shot
			{ spellID = 36554, filter = "CD" },										-- ShadowStep
			{ spellID = 51713, filter = "CD" },										-- Shadow Dance
			{ spellID = 79140, filter = "CD" },										-- Vendetta
			{ spellID = 1856, filter = "CD" },										-- Vanish
			{ spellID = 45182, filter = "ICD", trigger = "BUFF", duration = 90 },	-- Cheating Death ICD
		},
		{
			Name = "Player Buffs",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerPlayerBuff", 0, 22 },
			{ spellID = 13877, unitId = "player", caster = "player", filter = "BUFF" },		-- Blade Flurry
			{ spellID = 2983, unitId = "player", caster = "player", filter = "BUFF" },		-- Sprint
			{ spellID = 31224, unitId = "player", caster = "player", filter = "BUFF" },		-- Cloak of Shadows
			{ spellID = 13750, unitId = "player", caster = "player", filter = "BUFF" },		-- Adrenaline Rush
			{ spellID = 5277, unitId = "player", caster = "player", filter = "BUFF" },		-- Evasion
			{ spellID = 32645, unitId = "player", caster = "player", filter = "BUFF" },		-- Envenom
			{ spellID = 5171, unitId = "player", caster = "player", filter = "BUFF" },		-- Slice and Dice
			{ spellID = 73651, unitId = "player", caster = "player", filter = "BUFF" },		-- Recuperate
			{ spellID = 57934, unitId = "player", caster = "player", filter = "BUFF" },		-- Tricks of the Trade
			{ spellID = 51713, unitId = "player", caster = "player", filter = "BUFF" },		-- Shadow Dance
			{ spellID = 51690, unitId = "player", caster = "player", filter = "BUFF" },		-- Killing Spree
			{ spellID = 84745, unitId = "player", caster = "player", filter = "BUFF" },		-- Shallow Insight
			{ spellID = 84746, unitId = "player", caster = "player", filter = "BUFF" },		-- Moderate Insight
			{ spellID = 84747, unitId = "player", caster = "player", filter = "BUFF" },		-- Deep Insight
			{ spellID = 31665, unitId = "player", caster = "player", filter = "BUFF" },		-- Master of subtlety
			{ spellID = 74001, unitId = "player", caster = "player", filter = "BUFF" },		-- Combat Readiness
			{ spellID = 1966, unitId = "player", caster = "player", filter = "BUFF" },		-- Feint
			{ spellID = 114018, unitId = "player", caster = "player", filter = "BUFF" },	-- Shroud of Concealment
			{ spellID = 121471, unitId = "player", caster = "player", filter = "BUFF" },	-- Shadow Blades
			{ spellID = 108212, unitId = "player", caster = "player", filter = "BUFF" },	-- Burst of Speed
			{ spellID = 114842, unitId = "player", caster = "player", filter = "BUFF" },	-- Shadow Walk
			{ spellID = 109959, unitId = "player", caster = "player", filter = "BUFF" },	-- Suffering -- Legendary buff
			{ spellID = 109955, unitId = "player", caster = "player", filter = "BUFF" },	-- Nightmare -- Legendary buff
			{ spellID = 109941, unitId = "player", caster = "player", filter = "BUFF" },	-- Shadows of the Destroyer -- Legendary buff
		},
		{
			Name = "Target Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerTargetDebuff", 0, 22 },
			{ spellID = 1833, unitId = "target", caster = "all", filter = "DEBUFF" },					-- Cheap shot
			{ spellID = 408, unitId = "target", caster = "all", filter = "DEBUFF" },					-- Kidney shot
			{ spellID = 2094, unitId = "target", caster = "all", filter = "DEBUFF" },					-- Blind
			{ spellID = 6770, unitId = "target", caster = "all", filter = "DEBUFF" },					-- Sap
			{ spellID = 84617, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Revealing Strike
			{ spellID = 1943, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Rupture
			{ spellID = 703, unitId = "target", caster = "player", filter = "DEBUFF" },					-- Garrote
			{ spellID = 1776, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Gouge
			{ spellID = 122233, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Crimson Tempest
			{ spellID = 113746, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Weakened Armor
			{ spellID = 51722, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Dismantle
			{ spellID = 2818, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Deadly Poison
			{ spellID = 115194, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Mind Paralysis
			{ spellID = 115196, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Debilitating Poison
			{ spellID = 8680, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Wound Poison
			{ spellID = 91023, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Find Weakness
			{ spellID = 89775, unitId = "target", caster = "player", filter = "DEBUFF", absID = true },	-- Hemorrhage
			{ spellID = 79140, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Vendetta
			{ spellID = 112947, unitId = "target", caster = "player", filter = "DEBUFF" },				-- Nerve Strike
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
			Size = 32,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", "TOPRIGHT", 0, -22 },
			{ spellID = 2094, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Blind
			{ spellID = 6770, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Sap
			{ spellID = 1776, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Gouge
		},
		{
			Name = "Rogue Procs",
			Enable = true,
			Direction = "DOWN",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOP", "iFilgerProcs", -0, -23 },
			{ spellID = 57934, caster = "all", filter = "ACD", incombat = true },			-- Tricks of the Trade
			{ spellID = 109949, unitId = "player", caster = "player", filter = "BUFF" },	-- Fury of the Destroyer -- Legendary buff
			{ spellID = 1856, unitId = "player", caster = "player", filter = "BUFF" },		-- Vanish
			{ spellID = 45182, unitId = "player", caster = "player", filter = "BUFF" },		-- Cheating death
			{ spellID = 74002, unitId = "player", caster = "player", filter = "BUFF" },		-- Combat Insight
			{ spellID = 121153, unitId = "player", caster = "player", filter = "BUFF" },	-- Blindside
			{ spellID = 115192, unitId = "player", caster = "player", filter = "BUFF" },	-- Subterfuge
		},
	},
	["SHAMAN"] = {
		{
			Name = "Self Buffs",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
			Size = 32,
			setPoint = { "BOTTOMRIGHT", "iFilgerBuffPlayerHeal", 0, 24 },
			{ spellID = 974, unitId = "player", caster = "player", filter = "BUFF" },	-- Earth Shield
			{ spellID = 61295, unitId = "player", caster = "player", filter = "BUFF" },	-- Riptide
			{ spellID = 52127, unitId = "player", caster = "player", filter = "BUFF" },	-- Water Shield
		},
		{
			Name = "Target Buffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
			Size = 32,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffTargetHeal", 0, 24 },
			{ spellID = 974, unitId = "target", caster = "player", filter = "BUFF" },	-- Earth Shield
			{ spellID = 61295, unitId = "target", caster = "player", filter = "BUFF" },	-- Riptide
			{ spellID = 51945, unitId = "target", caster = "player", filter = "BUFF" },	-- Hearthliving
		},
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 108270, filter = "CD" },	-- Stone Bulwark Totem
			{ spellID = 8177, filter = "CD" },		-- Grounding Totem
			{ spellID = 8143, filter = "CD" },		-- Tremor Totem
			{ spellID = 98008, filter = "CD" },		-- Spirit Link Totem
			{ spellID = 79206, filter = "CD" },		-- Spiritwalker Grace
			{ spellID = 73680, filter = "CD" },		-- Unleash Elements
			{ spellID = 30823, filter = "CD" },		-- Shamanistic Rage
			{ spellID = 16190, filter = "CD" },		-- Mana Tide Totem
			{ spellID = 16188, filter = "CD" },		-- Ancestral Swiftness
			{ spellID = 2894, filter = "CD" },		-- Fire Elemental Totem
			{ spellID = 2062, filter = "CD" },		-- Earth Elemental Totem
			{ spellID = 2825, filter = "CD" },		-- Bloodlust
			{ spellID = 51514, filter = "CD" },		-- Hex
			{ spellID = 73920, filter = "CD" },		-- Healing Rain
		},
		{
			Name = "Shaman Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 16188, unitId = "player", caster = "player", filter = "BUFF" },		-- Ancestral Swiftness
			{ spellID = 53817, unitId = "player", caster = "player", filter = "BUFF" },		-- Maelstorm Weapon
			{ spellID = 51564, unitId = "player", caster = "player", filter = "BUFF" },		-- Tidal Waves
			{ spellID = 73685, unitId = "player", caster = "player", filter = "BUFF" },		-- Unleash Life
			{ spellID = 31616, unitId = "player", caster = "player", filter = "BUFF" },		-- Nature's Guardian
			{ spellID = 61295, caster = "all", filter = "ACD", incombat = false },			-- Riptide
			{ spellID = 5394, caster = "all", filter = "ACD", incombat = false, spec = 3 },	-- Healing Stream Totem
			{ spellID = 8178, unitId = "player", caster = "all", filter = "BUFF" },			-- Grounding Totem
			{ spellID = 114893, unitId = "player", caster = "player", filter = "BUFF" },	-- Stone Bulwark Totem
			{ spellID = 98007, unitId = "player", caster = "player", filter = "BUFF" },		-- Spirit Link Totem
			{ spellID = 105763, unitId = "player", caster = "player", filter = "BUFF" },	-- Spiritual Stimulus (2T13)
 			{ spellID = 105877, unitId = "player", caster = "player", filter = "BUFF" },	-- Timewalker (4T13 heal)
		},
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 974, unitId = "player", caster = "player", filter = "BUFF" },				-- Earth Shield
			{ spellID = 108271, unitId = "player", caster = "player", filter = "BUFF" },			-- Astral Shift
			{ spellID = 58875, unitId = "player", caster = "player", filter = "BUFF" },				-- Spirit Walk
			{ spellID = 108281, unitId = "player", caster = "player", filter = "BUFF" },			-- Ancestral Guidance
			{ spellID = 2645, unitId = "player", caster = "player", filter = "BUFF" },				-- Ghost Wolf
			{ spellID = 30823, unitId = "player", caster = "player", filter = "BUFF" },				-- Shamanistic Rage
			{ spellID = 77661, unitId = "target", caster = "all", filter = "DEBUFF", spec = 2 },	-- Totem
			{ spellID = 51514, unitId = "target", caster = "all", filter = "DEBUFF" },				-- Hex
			{ spellID = 76780, unitId = "target", caster = "all", filter = "DEBUFF" },				-- Bind Elemental
			{ spellID = 17364, unitId = "target", caster = "player", filter = "DEBUFF" },			-- Storm Strike
			{ spellID = 8042, unitId = "target", caster = "player", filter = "DEBUFF", spec = 2 },	-- Earth Shock
			{ spellID = 8056, unitId = "target", caster = "player", filter = "DEBUFF" },			-- Frost Shock
			{ spellID = 8050, unitId = "target", caster = "player", filter = "DEBUFF", spec = 2 },	-- Flame Shock
			{ spellID = 64695, unitId = "target", caster = "player", filter = "DEBUFF" },			-- Earthgrab
			{ spellID = 79206, unitId = "player", caster = "player", filter = "BUFF", spec = 3 },	-- Spiritwalker's Grace
		},
		{
			Name = "Focus",
			Enable = true,
			Direction = "LEFT",
--			IconSide = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
			Size = 32,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 974, unitId = "focus", caster = "player", filter = "BUFF" },	-- Earth Shield
			{ spellID = 51514, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Hex
			{ spellID = 76780, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Bind Elemental
		},
		{
			Name = "Debuffs Elem",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerTargetDebuff", 0, 22 },
			{ spellID = 8050, unitId = "target", caster = "player", filter = "DEBUFF", spec = 1 },	-- Flame Shock
			{ spellID = 77661, unitId = "target", caster = "player", filter = "DEBUFF", spec = 1 },	-- Searing Totem
			{ spellID = 51490, filter = "CD" },														-- Thunderstrom
			{ spellID = 8042, filter = "CD", spec = 1},												-- Earth Shock
		},
		{
			Name = "Buffs Elem",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerPlayerBuff", 0, 22 },
			{ spellID = 16246, unitId = "player", caster = "player", filter = "BUFF" },				-- Clearcasting
			{ spellID = 79206, unitId = "player", caster = "player", filter = "BUFF", spec = 1 },	-- Spiritwalker's Grace
			{ spellID = 16166, unitId = "player", caster = "player", filter = "BUFF" },				-- Elemental Mastery
		},
		{
			Name = "Lava Burst",
			Enable = true,
			Direction = "DOWN",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOP", "iFilgerLavaBurst", -0, -23 },
			{ spellID = 77762, unitId = "player", caster = "player", filter = "BUFF" },	-- Lava Surge
			{ spellID = 51505, filter = "CD", spec = 1 },								-- Lava Burst
		},
	},	
	["PALADIN"] = {
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 54428, filter = "CD" },	-- Divine Plea
			{ spellID = 633, filter = "CD" },	-- Lay on Hands
			{ spellID = 642, filter = "CD" },	-- Divine Shield
			{ spellID = 498, filter = "CD" },	-- Divine Protection
			{ spellID = 31884, filter = "CD" },	-- Avenging Wrath
			{ spellID = 86669, filter = "CD" },	-- Guardian of Ancient Kings
			{ spellID = 31842, filter = "CD" },	-- Divine Favor
			{ spellID = 31821, filter = "CD" },	-- Aura Mastery
			{ spellID = 1044, filter = "CD" },	-- Hand of Freedom
		},	
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerBuffPlayerHeal", 0, 24 },
			{ spellID = 53563, unitId = "player", caster = "player", filter = "BUFF" },	-- Beacon of Light
		},
		{
			Name = "Beacon of Light focus",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
			{ spellID = 53563, unitId = "focus", caster = "player", filter = "BUFF" },		-- Beacon of Light
			{ spellID = 853, unitId = "focus", caster = "all", filter = "DEBUFF" },			-- Hammer of Justice
			{ spellID = 20271, unitId = "focus", caster = "player", filter = "DEBUFF" },	-- Judgement
		},
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffTargetHeal", 0, 24 },
			{ spellID = 53563, unitId = "target", caster = "player", filter = "BUFF" },	-- Beacon of Light
		},
		{
			Name = "Buffs and Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 87173, unitId = "player", caster = "all", filter = "BUFF" },					-- Long Arm of the Law
			{ spellID = 53563, unitId = "target", caster = "player", filter = "BUFF" },					-- Beacon of Light
			{ spellID = 53657, unitId = "player", caster = "all", filter = "BUFF" },					-- Judgements of the Pure
			{ spellID = 853, unitId = "target", caster = "all", filter = "DEBUFF" },					-- Hammer of Justice
			{ spellID = 31803, unitId = "target", caster = "all", filter = "DEBUFF" },					-- Seal of Truth Aka Censure
			{ spellID = 31842, unitId = "player", caster = "player", filter = "BUFF" },					-- Divine Illumination
			{ spellID = 54428, unitId = "player", caster = "player", filter = "BUFF" },					-- Divine Plea
			{ spellID = 498, unitId = "player", caster = "player", filter = "BUFF" },					-- Divine Protection
			{ spellID = 84963, unitId = "player", caster = "player", filter = "BUFF" },					-- Inquisition
			{ spellID = 31850, unitId = "player", caster = "player", filter = "BUFF" },					-- Ardent Defender
			{ spellID = 31884, unitId = "player", caster = "player", filter = "BUFF" },					-- Avenging Wrath
			{ spellID = 86659, unitId = "player", caster = "player", filter = "BUFF" },					-- Guardian of Ancient Kings
			{ spellID = 31821, unitId = "player", caster = "all", filter = "BUFF" },					-- Aura Mastery
			{ spellID = 642, unitId = "player", caster = "player", filter = "BUFF" },					-- Divine Shield
			{ spellID = 82327, unitId = "player", caster = "player", filter = "BUFF" },					-- Holy Radiance
			{ spellID = 105801, unitId = "player", caster = "player", filter = "BUFF" },				-- Delayed Judgement - 2T13 Tank
			{ spellID = 105809, unitId = "player", caster = "player", filter = "BUFF" },				-- Holy Avenger
			{ spellID = 114637, unitId = "player", caster = "player", filter = "BUFF" },				-- Bastion of Glory
			{ spellID = 20925, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Sacred Shield
			{ spellID = 114163, unitId = "player", caster = "player", filter = "BUFF" },				-- Eternal Flame
			{ spellID = 121467, unitId = "player", caster = "player", filter = "BUFF" },				-- Alabaster Shield
		},
		{
			Name = "Paladin Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 85416, unitId = "player", caster = "player", filter = "BUFF" },		-- Grand Crusader
			{ spellID = 88819, unitId = "player", caster = "player", filter = "BUFF" },		-- Daybreak
			{ spellID = 53576, unitId = "player", caster = "player", filter = "BUFF" },		-- Infusion of Light
			{ spellID = 94686, unitId = "player", caster = "player", filter = "BUFF" },		-- Crusader
			{ spellID = 87138, unitId = "player", caster = "player", filter = "BUFF" },		-- Art of War
			{ spellID = 90174, unitId = "player", caster = "player", filter = "BUFF" },		-- Hand of Light
			{ spellID = 105742, unitId = "player", caster = "player", filter = "BUFF" },	-- Saint's Vigor - 2T13 Heal
			{ spellID = 114250, unitId = "player", caster = "player", filter = "BUFF" },	-- Selfless Healer
			{ spellID = 85499, unitId = "player", caster = "player", filter = "BUFF" },		-- Speed of Light
		},
		{
			Name = "Paly InvertAura",
			Enable = true,
			Direction = "HORIZONTAL",
			Interval = 4,
			Mode = "ICON",
			Alpha = 0.75,
--			BarWidth = 150,
			Size = 47,
			setPoint = { "BOTTOM", "iFilgerPalyInvertAura", 0, 22 },
			{ spellID = 84963, unitId = "player", caster = "player", filter = "IBUFF", incombat = true, spec = 3 },	-- Inquisition
		},
	},
	["WARRIOR"] = {
		{
			Name = "Warrior Procs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 60,
			setPoint = { "BOTTOMLEFT", "iFilgerProcs", 0, -63 },
			{ spellID = 52437, unitId = "player", caster = "player", filter = "BUFF" },					-- Sudden Death
			{ spellID = 46916, unitId = "player", caster = "all", filter = "BUFF" },					-- Slam!
			{ spellID = 50227, unitId = "player", caster = "player", filter = "BUFF" },					-- Sword and Board
			{ spellID = 64568, unitId = "player", caster = "player", filter = "BUFF" },					-- Blood Reserve
			{ spellID = 32216, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Victorious
			{ spellID = 23920, unitId = "player", caster = "player", filter = "BUFF" },					-- Spell Reflection
			{ spellID = 34428, unitId = "player", caster = "player", filter = "BUFF" },					-- Victory Rush
			{ spellID = 2565, unitId = "player", caster = "player", filter = "BUFF" },					-- Shield Block
			{ spellID = 12975, unitId = "player", caster = "player", filter = "BUFF" },					-- Last Stand
			{ spellID = 871, unitId = "player", caster = "player", filter = "BUFF" },					-- Shield Wall
			{ spellID = 122013, unitId = "player", caster = "player", filter = "BUFF" },				-- Incite
			{ spellID = 112048, unitId = "player", caster = "player", filter = "BUFF" },				-- Shield Barrier
			{ spellID = 56638, unitId = "player", caster = "player", filter = "BUFF"},					-- Taste for Blood
			{ spellID = 114028, unitId = "player", caster = "player", filter = "BUFF" },				-- Mass Spell Reflection
			{ spellID = 97462, unitId = "player", caster = "all", filter = "BUFF" }, 					-- Rallying Cry
		},
		{
			Name = "Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerBuffDebuff", 0, 24 },
			{ spellID = 1715, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Hamstring
			{ spellID = 7386, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Sunder Armor
			{ spellID = 6343, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Thunder Clap
			{ spellID = 1160, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Demoralizing Shout
			{ spellID = 7922, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Charge Stun
			{ spellID = 46968, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Shockwave
			{ spellID = 8647, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Expose Armor
			{ spellID = 86346, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Colossus Smash
			{ spellID = 115768, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Deep Wounds
			{ spellID = 113746, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Weakened Armor
			{ spellID = 115798, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Thunderclap
		},
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 1719, filter = "CD" },		-- Recklesness
			{ spellID = 18499, filter = "CD" },		-- Berserker rage
			{ spellID = 12975, filter = "CD" },		-- Last Stand
			{ spellID = 355, filter = "CD" },		-- Taunt
			{ spellID = 469, filter = "CD" },		-- Commanding Shout
			{ spellID = 6544, filter = "CD" },		-- Heroic Leap
			{ spellID = 118000, filter = "CD" },	-- Dragon Roar
		},
	},
	["MONK"] = {
		{
			Name = "Buffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerPlayerBuff", 0, 22 },
			{ spellID = 121125, unitId = "player", caster = "player", filter = "BUFF" },	-- Death Note
			{ spellID = 120954, unitId = "player", caster = "player", filter = "BUFF" },	-- Fortifying Brew
			{ spellID = 125359, unitId = "player", caster = "player", filter = "BUFF" },	-- Tiger Power
			{ spellID = 124336, unitId = "player", caster = "player", filter = "BUFF" },	-- Path of Blossoms
			{ spellID = 101546, unitId = "player", caster = "player", filter = "BUFF" },	-- Spinning Crane Kick
			{ spellID = 131523, unitId = "player", caster = "player", filter = "BUFF" },	-- Zen Meditation
			{ spellID = 115288, unitId = "player", caster = "player", filter = "BUFF" },	-- Energizing Brew
			{ spellID = 125195, unitId = "player", caster = "player", filter = "BUFF" },	-- Tigereye Brew
			{ spellID = 120273, unitId = "player", caster = "player", filter = "BUFF" },	-- Tiger Strikes
		},
		{
			Name = "Debuffs",
			Enable = true,
			Direction = "RIGHT",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMLEFT", "iFilgerTargetDebuff", 0, 22 },
			{ spellID = 119381, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Leg Sweep
			{ spellID = 115078, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Paralysis
			{ spellID = 116095, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Disable
		},
		{
			Name = "Cooldown",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 115072, filter = "CD" },	-- Expel Harm
			{ spellID = 115203, filter = "CD" },	-- Forifying Brew
			{ spellID = 117368, filter = "CD" },	-- Grapple Weapon
			{ spellID = 115078, filter = "CD" },	-- Paralysis
			{ spellID = 116705, filter = "CD" },	-- Spear Hand Strike
			{ spellID = 115080, filter = "CD" },	-- Touch of Death
			{ spellID = 115176, filter = "CD" },	-- Zen Meditation
			{ spellID = 115288, filter = "CD" },	-- Energizing Brew
			{ spellID = 113656, filter = "CD" },	-- Fists of Fury
			{ spellID = 101545, filter = "CD" },	-- Flying Serpent Kick
			{ spellID = 122470, filter = "CD" },	-- Touch of Karma
			{ spellID = 115315, filter = "CD" },	-- Summon Black Ox
			{ spellID = 115213, filter = "CD" },	-- Avert Harm
			{ spellID = 115295, filter = "CD" },	-- Guard
			{ spellID = 122057, filter = "CD" },	-- Clash
			{ spellID = 115310, filter = "CD" },	-- Revival
			{ spellID = 115313, filter = "CD" },	-- Summon Jade Serpent Statue
			{ spellID = 116680, filter = "CD" },	-- Thunder Focus Tea
			{ spellID = 116849, filter = "CD" },	-- Life Cocoon
			{ spellID = 122783, filter = "CD" },	-- Diffuse Magic
			{ spellID = 122278, filter = "CD" },	-- Dampen Harm
			{ spellID = 119381, filter = "CD" },	-- Leg Sweep
			{ spellID = 107428, filter = "CD" },	-- Rising Sun Kick, Windwalker
			{ spellID = 132467, filter = "CD" },	-- Chi Wave, Talent
			{ spellID = 116841, filter = "CD" },	-- Tiger's Lust, Talent
			{ spellID = 119611, filter = "CD" },	-- Renewing Mist, Mistweaver
			{ spellID = 115294, filter = "CD" },	-- Mana Tea, Mistweaver
		},
	},			
	["HUNTER/DRUID/ROGUE"] = {
		{
			Name = "Shivable Rage effects",
			Enable = true,
			Direction = "UP",
			IconSide = "LEFT",
			Interval = 4,
			Mode = "ICON",
			Alpha = 1,
			Size = 40,
			setPoint = { "BOTTOM", "iFilgerRage", "BOTTOM", 0, 22 },
			{ spellID = 49016, unitId = "target", caster = "all", filter = "BUFF" },	-- Unholy Frenzy
			{ spellID = 76691, unitId = "target", caster = "all", filter = "BUFF" },	-- Vengeance
			{ spellID = 5229, unitId = "target", caster = "all", filter = "BUFF" },		-- Enrage
			{ spellID = 52610, unitId = "target", caster = "all", filter = "BUFF" },	-- Savage Roar
			{ spellID = 48391, unitId = "target", caster = "all", filter = "BUFF" },	-- Owlkin Frenzy
			{ spellID = 18499, unitId = "target", caster = "all", filter = "BUFF" },	-- Berserker Rage
			{ spellID = 12292, unitId = "target", caster = "all", filter = "BUFF" },	-- Death Wish
			{ spellID = 84608, unitId = "target", caster = "all", filter = "BUFF" },	-- Bastion of Defense
		},
	},
	["ALL"] = {
		{
			Name = "ENHANCEMENTS",
			Enable = true,
			Direction = "LEFT",
			Interval = 6,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 52,
			setPoint = { "TOPRIGHT", "iFilgerEnhancements", 0, -22 },
		-- Racials
			{ spellID = 26297, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Berserking
			{ spellID = 33697, unitId = "player", caster = "player", filter = "BUFF" },					-- Blood Fury
			{ spellID = 68992, unitId = "player", caster = "player", filter = "BUFF" },					-- Darkflight
		-- Trinket Procs
			-- 333/346
			{ spellID = 91139, unitId = "player", caster = "player", filter = "BUFF" },					-- Tear of Blood
			{ spellID = 90887, unitId = "player", caster = "player", filter = "BUFF" },					-- Witching Hour
			-- 359/372/379
			{ spellID = 91192, unitId = "player", caster = "all", filter = "BUFF" },  					-- Mandala of Stirring Patterns
			{ spellID = 92222, unitId = "player", caster = "all", filter = "BUFF" },					-- Mirror of Broken Images
			{ spellID = 91007, unitId = "player", caster = "all", filter = "BUFF" }, 					-- Bell of Enraging Resonance
			{ spellID = 91047, unitId = "player", caster = "all", filter = "BUFF" }, 					-- Stump of Time
			{ spellID = 89091, unitId = "player", caster = "player", filter = "BUFF" },					-- Volcanic Destruction
			{ spellID = 92345, unitId = "player", caster = "player", filter = "BUFF" },					-- Heart of Rage
			{ spellID = 91027, unitId = "player", caster = "player", filter = "BUFF" },					-- Heart's Revelation
			{ spellID = 91041, unitId = "player", caster = "player", filter = "BUFF" },					-- Heart's Judgement
			{ spellID = 91019, unitId = "player", caster = "player", filter = "BUFF" },					-- Soul Casket
			{ spellID = 91024, unitId = "player", caster = "player", filter = "BUFF" },					-- Theralion Mirror
			{ spellID = 91173, unitId = "player", caster = "player", filter = "BUFF" },					-- Shard of Woe
			-- 365/378/384/391/397
			{ spellID = 96962, unitId = "player", caster = "player", filter = "BUFF" },					-- Necromatic Focus
			{ spellID = 101156, unitId = "player", caster = "player", filter = "BUFF" },				-- Moonwell Phial
			{ spellID = 100403, unitId = "player", caster = "player", filter = "BUFF" },				-- Moonwell Chalice
			{ spellID = 101515, unitId = "player", caster = "player", filter = "BUFF" },				-- Ricket's Magnetic Fireball
			{ spellID = 96976, unitId = "player", caster = "player", filter = "BUFF" },					-- Matrix Restabilizer
			{ spellID = 96980, unitId = "player", caster = "player", filter = "BUFF" },					-- Accelerated
			{ spellID = 97009, unitId = "player", caster = "player", filter = "BUFF" },					-- Ancient Petrified Seed
			{ spellID = 96934, unitId = "player", caster = "player", filter = "BUFF" },					-- Apparatus of Khaz'goroth
			{ spellID = 97010, unitId = "player", caster = "player", filter = "BUFF" },					-- Essence of the Eternal Flame
			{ spellID = 97008, unitId = "player", caster = "player", filter = "BUFF" },					-- Fiery Quintessence
			{ spellID = 96907, unitId = "player", caster = "player", filter = "BUFF", absID = true },	-- Jaws of Defeat
			{ spellID = 97007, unitId = "player", caster = "player", filter = "BUFF" },					-- Rune of Zeth
			{ spellID = 96911, unitId = "player", caster = "player", filter = "BUFF" },					-- The Hungerer
			{ spellID = 96879, unitId = "player", caster = "player", filter = "BUFF" },					-- Scales of Life
			{ spellID = 96923, unitId = "player", caster = "player", filter = "BUFF" },					-- Apparatus of Khaz'goroth
			{ spellID = 101291, unitId = "player", caster = "player", filter = "BUFF" },				-- Mithril Stopwatch
			{ spellID = 101293, unitId = "player", caster = "player", filter = "BUFF" },				-- Brawler's Trophy
			{ spellID = 101289, unitId = "player", caster = "player", filter = "BUFF" },				-- Petrified Pickled Egg
			{ spellID = 96945, unitId = "player", caster = "player", filter = "BUFF" },					-- Spidersilk Spindle
			{ spellID = 96988, unitId = "player", caster = "player", filter = "BUFF" },					-- Stay of Execution
			-- 378/384/397/401
			{ spellID = 102662, unitId = "player", caster = "player", filter = "BUFF", absID = true },		-- Foul Gift of the Demon Lord
			{ spellID = 109908, unitId = "player", caster = "player", filter = "DEBUFF", absID = true },	-- Foul Gift of the Demon Lord
			{ spellID = 109750, unitId = "player", caster = "player", filter = "BUFF" },					-- Eye of Unmaking/Rotting Skull
			{ spellID = 109813, unitId = "player", caster = "player", filter = "BUFF" },					-- Heart of Unliving
			{ spellID = 109782, unitId = "player", caster = "player", filter = "BUFF" },					-- Resolve of Undying
			{ spellID = 109795, unitId = "player", caster = "player", filter = "BUFF" },					-- Will of Unbinding
			{ spellID = 109719, unitId = "player", caster = "player", filter = "BUFF" },					-- Wrath of Unchaining
			{ spellID = 109744, unitId = "player", caster = "player", filter = "BUFF" },					-- Creche of the Final Dragon
			{ spellID = 109792, unitId = "player", caster = "player", filter = "BUFF" },					-- Bottled Wishes/Reflection of the Light
			{ spellID = 109779, unitId = "player", caster = "player", filter = "BUFF" },					-- Fire of the Deep
			{ spellID = 109789, unitId = "player", caster = "player", filter = "BUFF" },					-- Insignia of the Corrupted Mind/Seal of the Seven Signs
			{ spellID = 109715, unitId = "player", caster = "player", filter = "BUFF" },					-- Kiroptyric Sigil
			{ spellID = 109776, unitId = "player", caster = "player", filter = "BUFF" },					-- Soulshifter Vortex
			{ spellID = 107804, unitId = "player", caster = "all", filter = "BUFF" },						-- Slowing the Sands
			{ spellID = 109863, unitId = "player", caster = "player", filter = "BUFF" },					-- Beast Fury
		-- PvP Trinkets
			{ spellID = 99711, unitId = "player", caster = "player", filter = "BUFF" },	-- Call of Conquest / Badge of Conquest
			{ spellID = 99712, unitId = "player", caster = "player", filter = "BUFF" },	-- Call of Dominance / Badge of Dominance
			{ spellID = 99713, unitId = "player", caster = "player", filter = "BUFF" },	-- Call of Victory / Badge of Victory
			{ spellID = 99714, unitId = "player", caster = "player", filter = "BUFF" },	-- Tremendous Fortitude / Battlemaster Trinket S10-371
			{ spellID = 99717, unitId = "player", caster = "player", filter = "BUFF" },	-- Surge of Conquest / Insignia of Conquest
			{ spellID = 99719, unitId = "player", caster = "player", filter = "BUFF" },	-- Surge of Dominance / Insignia of Dominance
			{ spellID = 99721, unitId = "player", caster = "player", filter = "BUFF" },	-- Surge of Victory / Insignia of Victory
		-- Item Enchants
			{ spellID = 54758, unitId = "player", caster = "player", filter = "BUFF" },	-- Hyperspeed Accelerators
			{ spellID = 96230, unitId = "player", caster = "player", filter = "BUFF" },	-- Synapse Springs
			{ spellID = 54861, unitId = "player", caster = "player", filter = "BUFF" },	-- Nitro Boots
			{ spellID = 82626, unitId = "player", caster = "player", filter = "BUFF" },	-- Grounded Plasma Shield 
			{ spellID = 99621, unitId = "player", caster = "player", filter = "BUFF" },	-- Flintlocke's Woodchucker
			{ spellID = 55503, unitId = "player", caster = "player", filter = "BUFF" },	-- Lifeblood
			{ spellID = 74224, unitId = "player", caster = "all", filter = "BUFF" },	-- Heartsong
			{ spellID = 74196, unitId = "player", caster = "all", filter = "BUFF" },	-- Avalanche
			{ spellID = 74221, unitId = "player", caster = "all", filter = "BUFF" },	-- Hurricane
			{ spellID = 74241, unitId = "player", caster = "all", filter = "BUFF" },	-- Power Torrent
			{ spellID = 74245, unitId = "player", caster = "all", filter = "BUFF" },	-- Landside
		-- Potions
			{ spellID = 53908, unitId = "player", caster = "all", filter = "BUFF" },	-- Potion of Speed
			{ spellID = 53909, unitId = "player", caster = "all", filter = "BUFF" },	-- Potion of Wild Magic
			{ spellID = 79475, unitId = "player", caster = "all", filter = "BUFF" },	-- Earthen Potion
			{ spellID = 79476, unitId = "player", caster = "all", filter = "BUFF" }, 	-- Volcanic Power
			{ spellID = 79634, unitId = "player", caster = "player", filter = "BUFF" },	-- Golemblood Potion
		-- External Buffs
			{ spellID = 57934, unitId = "player", caster = "all", filter = "BUFF" },	-- Tricks of the Trade
			{ spellID = 10060, unitId = "player", caster = "all", filter = "BUFF" },	-- Power Infusion
			{ spellID = 65081, unitId = "player", caster = "all", filter = "BUFF" },	-- Body and Soul
			{ spellID = 121557, unitId = "player", caster = "all", filter = "BUFF" },	-- Angelic Feather
			{ spellID = 2825, unitId = "player", caster = "all", filter = "BUFF" },		-- Bloodlust
			{ spellID = 32182, unitId = "player", caster = "all", filter = "BUFF" },	-- Heroism
			{ spellID = 80353, unitId = "player", caster = "all", filter = "BUFF" },	-- Time Warp
			{ spellID = 90355, unitId = "player", caster = "all", filter = "BUFF" },	-- Ancient Hysteria
			{ spellID = 29166, unitId = "player", caster = "all", filter = "BUFF"},		-- Innervate
			{ spellID = 7001, unitId = "player", caster = "all", filter = "BUFF"},		-- Lightwell
			{ spellID = 1038, unitId = "player", caster = "all", filter = "BUFF" },		-- Hand of Salvation
			{ spellID = 1022, unitId = "player", caster = "all", filter = "BUFF" },		-- Hand of Protection
			{ spellID = 1044, unitId = "player", caster = "all", filter = "BUFF" },		-- Hand of Freedom
			{ spellID = 47788, unitId = "player", caster = "all", filter = "BUFF" },	-- Guardian Spirit
			{ spellID = 13159, unitId = "player", caster = "all", filter = "BUFF" },	-- Aspect of the Pack
			{ spellID = 77764, unitId = "player", caster = "all", filter = "BUFF" },	-- Stampeding Roar
			{ spellID = 54216, unitId = "player", caster = "all", filter = "BUFF" },	-- Master's Call
			{ spellID = 24378, unitId = "player", caster = "all", filter = "BUFF", absID = true },	-- Berserking
		},
		{
			Name = "Racials",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
			Merge = true,
			Mergewith = "Cooldown",
--			BarWidth = 150,
			Size = 52,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },

			{ slotID = 13, filter = "CD" }, 		-- Trinket 1
			{ slotID = 14, filter = "CD" }, 		-- Trinket 2
			{ spellID = 28730, filter = "CD" }, 	-- Arcane Torrent
			{ spellID = 59547, filter = "CD" }, 	-- Gift of the Naaru
			{ spellID = 20594, filter = "CD" }, 	-- Stoneform
			{ spellID = 58984, filter = "CD" }, 	-- Shadowmeld
			{ spellID = 20572, filter = "CD" }, 	-- Blood Fury
			{ spellID = 68992, filter = "CD" }, 	-- Darkflight
			{ spellID = 20577, filter = "CD" }, 	-- Cannibalize
			{ spellID = 7744, filter = "CD" }, 		-- Will of the Forsaken
			{ spellID = 26297, filter = "CD" }, 	-- Berserking
			{ spellID = 59752, filter = "CD" }, 	-- Every Man for Himself
			{ spellID = 69070, filter = "CD" }, 	-- Rocket Jump
			{ spellID = 20589, filter = "CD" }, 	-- Escape Artist
		},
		{
			Name = "ICD",
			Enable = true,
			Direction = "UP",
			Interval = 3,
			Mode = "ICON",
			Alpha = 1,
			Merge = true,
			Mergewith = "Cooldown",
--			BarWidth = 150,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerCooldowns", 40, 0 },
			{ spellID = 102662, filter = "ICD", trigger = "BUFF", duration = 45, absID = true },	-- Foul Gift of the Demon Lord
			{ spellID = 108008, filter = "ICD", trigger = "BUFF", duration = 60 },					-- Indomitable Pride
			{ spellID = 109744, filter = "ICD", trigger = "BUFF", duration = 115 },					-- Creche of the Final Dragon
			{ spellID = 109776, filter = "ICD", trigger = "BUFF", duration = 115 },					-- Soulshifter Vortex
			{ spellID = 109789, filter = "ICD", trigger = "BUFF", duration = 115 }, 				-- Starcatcher Compass, Insignia of the Corrupted Mind, Seal of the Seven Signs
			{ spellID = 74241, filter = "ICD", trigger = "BUFF", duration = 45 }, 					-- Power Torrent
			{ spellID = 57934, filter = "ICD", trigger = "BUFF", duration = 30 },					-- Tricks of the Trade
			{ spellID = 74241, filter = "ICD", trigger = "BUFF", duration = 45, slotID = 16 },
		},
	},
	["PVP"] = {
		{
			Name = "ENEMY PVP BUFF",
			Enable = true,
			Direction = "RIGHT",
			Interval = 6,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 72,
			setPoint = { "BOTTOMLEFT", "iFilgerPvpTargetBuffs", 0, 22 },
		-- Druid
			{ spellID = 29166, unitId = "target", caster = "all", filter = "BUFF"},					-- Innervate
			{ spellID = 22812, unitId = "target", caster = "all", filter = "BUFF" },				-- Barkskin
			{ spellID = 61336, unitId = "target", caster = "all", filter = "BUFF" },				-- Survival Instincts
			{ spellID = 22842, unitId = "target", caster = "all", filter = "BUFF" },				-- Frenzied Regeneration
			{ spellID = 16689, unitId = "target", caster = "all", filter = "BUFF"},					-- Nature's Grasp
			{ spellID = 106898, unitId = "target", caster = "all", filter = "BUFF" },				-- Stampeding Roar
			{ spellID = 106951, unitId = "target", caster = "all", filter = "BUFF", absID = true },	-- Berserk
			{ spellID = 48505, unitId = "target", caster = "all", filter = "BUFF" },				-- Starfall
			{ spellID = 69369, unitId = "target", caster = "all", filter = "BUFF" },				-- Predatory Swiftness
			{ spellID = 117679, unitId = "target", caster = "all", filter = "BUFF" },				-- Incarnation
			{ spellID = 102543, unitId = "target", caster = "all", filter = "BUFF" },				-- Incarnation: King of the Jungle
			{ spellID = 102558, unitId = "target", caster = "all", filter = "BUFF" },				-- Incarnation: Son of Ursoc
			{ spellID = 102560, unitId = "target", caster = "all", filter = "BUFF" },				-- Incarnation: Chosen of Elune
			{ spellID = 102342, unitId = "target", caster = "all", filter = "BUFF" },				-- Ironbark
			{ spellID = 106922, unitId = "target", caster = "all", filter = "BUFF" },				-- Might of Ursoc
			{ spellID = 112071, unitId = "target", caster = "all", filter = "BUFF" },				-- Celestial Alignment
			{ spellID = 124974, unitId = "target", caster = "all", filter = "BUFF" },				-- Nature's Vigil
		-- Warrior
			{ spellID = 23920, unitId = "target", caster = "all", filter = "BUFF" },				-- Spell Reflection
			{ spellID = 55694, unitId = "target", caster = "all", filter = "BUFF" },				-- Enraged Regeneration
			{ spellID = 97463, unitId = "target", caster = "all", filter = "BUFF" },				-- Rallying Cry
			{ spellID = 871, unitId = "target", caster = "all", filter = "BUFF" },					-- Shield Wall
			{ spellID = 18499, unitId = "target", caster = "all", filter = "BUFF" },				-- Berserker Rage
			{ spellID = 2565, unitId = "target", caster = "all", filter = "BUFF" },					-- Shield Block
			{ spellID = 12975, unitId = "target", caster = "all", filter = "BUFF" },				-- Last Stand
			{ spellID = 1719, unitId = "target", caster = "all", filter = "BUFF" },					-- Recklessness
			{ spellID = 46924, unitId = "target", caster = "all", filter = "BUFF" },				-- Bladestorm
			{ spellID = 118038, unitId = "target", caster = "all", filter = "BUFF" },				-- Die by the Sword
			{ spellID = 114029, unitId = "target", caster = "all", filter = "BUFF" },				-- Safeguard
			{ spellID = 114028, unitId = "target", caster = "all", filter = "BUFF" },				-- Mass Spell Reflection
			{ spellID = 3411, unitId = "target", caster = "all", filter = "BUFF" },					-- Intervene
			{ spellID = 114030, unitId = "target", caster = "all", filter = "BUFF" },				-- Vigilance
			{ spellID = 107574, unitId = "target", caster = "all", filter = "BUFF" },				-- Avatar
		-- Paladin
			{ spellID = 25771, unitId = "target", caster = "all", filter = "DEBUFF" },				-- Forbearance
			{ spellID = 642, unitId = "target", caster = "all", filter = "BUFF" },					-- Divine Shield
			{ spellID = 1044, unitId = "target", caster = "all", filter = "BUFF" },					-- Hand of Freedom
			{ spellID = 6940, unitId = "target", caster = "all", filter = "BUFF" },					-- Hand of Sacrifice
			{ spellID = 31821, unitId = "target", caster = "all", filter = "BUFF" },				-- Devotion Aura
			{ spellID = 1022, unitId = "target", caster = "all", filter = "BUFF" },					-- Hand of Protection
			{ spellID = 498, unitId = "target", caster = "all", filter = "BUFF" },					-- Divine Protection
			{ spellID = 31884, unitId = "target", caster = "all", filter = "BUFF" },				-- Avenging Wrath
			{ spellID = 31842, unitId = "target", caster = "all", filter = "BUFF" },				-- Divine Favor
			--{ spellID = 70940, unitId = "target", caster = "all", filter = "BUFF" },				-- Divine Guardian
			{ spellID = 31850, unitId = "target", caster = "all", filter = "BUFF" },				-- Ardent Defender
			{ spellID = 86659, unitId = "target", caster = "all", filter = "BUFF" },				-- Guardian of Ancient Kings
			{ spellID = 20925, unitId = "target", caster = "all", filter = "BUFF", absID = true },	-- Sacred Shield
			{ spellID = 54428, unitId = "target", caster = "all", filter = "BUFF" },				-- Divine Plea
			{ spellID = 114039, unitId = "target", caster = "all", filter = "BUFF" },				-- Hand of Purity
			{ spellID = 105809, unitId = "target", caster = "all", filter = "BUFF" },				-- Holy Avenger
			{ spellID = 85499, unitId = "target", caster = "all", filter = "BUFF" },				-- Speed of Light
		-- Hunter
			{ spellID = 19263, unitId = "target", caster = "all", filter = "BUFF" },				-- Deterrence
			{ spellID = 54216, unitId = "target", caster = "all", filter = "BUFF" },				-- Master's Call
			{ spellID = 34471, unitId = "target", caster = "all", filter = "BUFF" },				-- The Beast Within
			{ spellID = 3045, unitId = "target", caster = "all", filter = "BUFF" },					-- Rapid Fire
			{ spellID = 53480, unitId = "target", caster = "all", filter = "BUFF" },				-- Roar of Sacrifice
			{ spellID = 13159, unitId = "target", caster = "all", filter = "BUFF" },				-- Aspect of the Pack
			{ spellID = 5118, unitId = "target", caster = "all", filter = "BUFF" },					-- Aspect of the Cheetah
		-- Death Knight
			{ spellID = 48707, unitId = "target", caster = "all", filter = "BUFF" },				-- Anti-Magic Shell
			{ spellID = 49222, unitId = "target", caster = "all", filter = "BUFF" }, 				-- Bone Shield
			{ spellID = 49039, unitId = "target", caster = "all", filter = "BUFF" },				-- Lichborne
			{ spellID = 55233, unitId = "target", caster = "all", filter = "BUFF" },				-- Vampiric Blood
			{ spellID = 48792, unitId = "target", caster = "all", filter = "BUFF" },				-- Icebound Fortitude
			{ spellID = 51271, unitId = "target", caster = "all", filter = "BUFF" },				-- Pillar of Frost
			{ spellID = 96268, unitId = "target", caster = "all", filter = "BUFF" },				-- Death's Advance
		-- Shaman
			{ spellID = 8178, unitId = "target", caster = "all", filter = "BUFF" },					-- Grounding Totem Effect
			{ spellID = 30823, unitId = "target", caster = "all", filter = "BUFF" },				-- Shamanistic Rage
			{ spellID = 974, unitId = "target", caster = "all", filter = "BUFF" },					-- Earth Shield
			{ spellID = 79206, unitId = "target", caster = "all", filter = "BUFF" },				-- Spiritwalker's Grace
			{ spellID = 16191, unitId = "target", caster = "all", filter = "BUFF" },				-- Mana Tide
			{ spellID = 58875, unitId = "target", caster = "all", filter = "BUFF" },				-- Spirit Walk
			{ spellID = 108281, unitId = "target", caster = "all", filter = "BUFF" },				-- Ancestral Guidance
			{ spellID = 108271, unitId = "target", caster = "all", filter = "BUFF" },				-- Astral Shift
			{ spellID = 16166, unitId = "target", caster = "all", filter = "BUFF" },				-- Elemental Mastery
			{ spellID = 114896, unitId = "target", caster = "all", filter = "BUFF" },				-- Windwalk Totem
		-- Mage
			{ spellID = 45438, unitId = "target", caster = "all", filter = "BUFF" },				-- Ice Block
			{ spellID = 12042, unitId = "target", caster = "all", filter = "BUFF" },				-- Arcane Power
			{ spellID = 12472, unitId = "target", caster = "all", filter = "BUFF" },				-- Icy Veins
			--{ spellID = 83074, unitId = "target", caster = "all", filter = "BUFF"},					-- Fingers Of Frost
			{ spellID = 32612, unitId = "target", caster = "all", filter = "BUFF" },				-- Invisibility
			{ spellID = 12051, unitId = "target", caster = "all", filter = "BUFF" },				-- Evocation
			{ spellID = 108843, unitId = "target", caster = "all", filter = "BUFF" },				-- Blazing Speed
			{ spellID = 87023, unitId = "target", caster = "target", filter = "DEBUFF" },			-- Cauterize
			{ spellID = 110909, unitId = "target", caster = "all", filter = "BUFF" },				-- Alter Time
			{ spellID = 108839, unitId = "target", caster = "all", filter = "BUFF" },				-- Ice Floes
			{ spellID = 111264, unitId = "target", caster = "all", filter = "BUFF" },				-- Ice Ward
		-- Rogue
			{ spellID = 31224, unitId = "target", caster = "all", filter = "BUFF" },				-- Cloak of Shadows
			{ spellID = 5277, unitId = "target", caster = "all", filter = "BUFF" },					-- Evasion
			{ spellID = 51713, unitId = "target", caster = "all", filter = "BUFF" },				-- Shadow Dance
			{ spellID = 74002, unitId = "target", caster = "all", filter = "BUFF" },				-- Combat Insight
			{ spellID = 74001, unitId = "target", caster = "all", filter = "BUFF" },				-- Combat Readiness
			{ spellID = 45182, unitId = "target", caster = "all", filter = "BUFF" },				-- Cheating Death
		-- Priest
			{ spellID = 47585, unitId = "target", caster = "all", filter = "BUFF" },				-- Dispersion
			{ spellID = 33206, unitId = "target", caster = "all", filter = "BUFF" },				-- Pain Suppression
			{ spellID = 10060, unitId = "target", caster = "all", filter = "BUFF" },				-- Power Infusion
			{ spellID = 96267, unitId = "target", caster = "all", filter = "BUFF" },				-- Glyph of Inner Focus
			{ spellID = 47788, unitId = "target", caster = "all", filter = "BUFF" },				-- Guardian Spirit
			{ spellID = 6346, unitId = "target", caster = "all", filter = "BUFF" },					-- Fear Ward
			{ spellID = 114239, unitId = "target", caster = "all", filter = "BUFF" },				-- Phantasm
		-- Warlock
			{ spellID = 113860, unitId = "player", caster = "all", filter = "BUFF" },				-- Dark Soul: Misery
			{ spellID = 113858, unitId = "player", caster = "all", filter = "BUFF" },				-- Dark Soul: Instability
			{ spellID = 113861, unitId = "player", caster = "all", filter = "BUFF" },				-- Dark Soul: Knowledge
			{ spellID = 104773, unitId = "target", caster = "all", filter = "BUFF" },				-- Unending Resolve
			{ spellID = 111400, unitId = "target", caster = "all", filter = "BUFF" },				-- Burning Rush
			{ spellID = 110913, unitId = "target", caster = "all", filter = "BUFF" },				-- Dark Bargain
			{ spellID = 108359, unitId = "target", caster = "all", filter = "BUFF" },				-- Dark Regeneration
			{ spellID = 88448, unitId = "target", caster = "all", filter = "BUFF" },				-- Demonic Rebirth
			{ spellID = 20707, unitId = "target", caster = "all", filter = "BUFF" },				-- Soulstone
			{ spellID = 114168, unitId = "target", caster = "all", filter = "BUFF" },				-- Dark Apotheosis
			{ spellID = 103958, unitId = "target", caster = "all", filter = "BUFF" },				-- Metamorphosis
		-- All
			{ spellID = 23333, unitId = "target", caster = "all", filter = "BUFF" },				-- Warsong Flag
			{ spellID = 23335, unitId = "target", caster = "all", filter = "BUFF" },				-- Warsong Flag
			{ spellID = 34976, unitId = "target", caster = "all", filter = "BUFF" },				-- Netherstorm Flag
			{ spellID = 52418, unitId = "target", caster = "all", filter = "BUFF" },				-- Seaforium
			{ spellID = 66271, unitId = "target", caster = "all", filter = "DEBUFF" },				-- Seaforium
			{ spellID = 80167, unitId = "target", caster = "all", filter = "BUFF" },				-- Drink
		},
		{
			Name = "ENEMY PVP DEBUFF",
			Enable = true,
			Direction = "LEFT",
			Interval = 6,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Size = 70,
			setPoint = { "BOTTOMRIGHT", "iFilgerPvpPlayerDebuffs" ,0,22 },
		-- Rogue
			{ spellID = 408, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Kidney Shot
			{ spellID = 2094, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Blind
			{ spellID = 6770, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Sap
			{ spellID = 88611, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Smoke Bomb
			{ spellID = 1330, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Garrote
			{ spellID = 51722, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Dismantle
			{ spellID = 3409, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Crippling Poison
			{ spellID = 119696, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Debilitation
			{ spellID = 26679, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Deadly Throw
			{ spellID = 1776, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Gouge
			{ spellID = 1833, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Cheap Shot
			{ spellID = 79140, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Vendetta
			{ spellID = 5760, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Mind-Numbing Poison
			{ spellID = 115197, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Partial Paralytic
			{ spellID = 113953, unitId = "player", caster = "all", filter = "DEBUFF", absID = true }, 	-- Paralysis
		-- Paladin
			{ spellID = 853, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Hammer of Justice
			{ spellID = 20170, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Seal of Justice
			{ spellID = 20066, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Repentance
			{ spellID = 119072, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Holy Wrath
			{ spellID = 10326, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Turn Evil
			{ spellID = 31935, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Avenger's Shield
			{ spellID = 105593, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Fist of Justice
			{ spellID = 105421, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Blinding Light
			{ spellID = 2812, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Denounce
			{ spellID = 110300, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Burden of Guilt
		-- Mage
			{ spellID = 55021, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Improved Counterspell
			{ spellID = 118, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Polymorph
			{ spellID = 44572, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Deep Freeze
			{ spellID = 82691, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Ring of Frost
			{ spellID = 33395, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Freeze
			{ spellID = 122, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frost Nova
			{ spellID = 11113, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Blast Wave
			{ spellID = 120, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Cone of Cold
			{ spellID = 116, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frostbolt
			{ spellID = 44614, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frostfire Bolt
			{ spellID = 31589, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Slow
			{ spellID = 31661, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Dragon's Breath
			{ spellID = 87023, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Cauterize
			{ spellID = 118271, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Combustion Impact
			{ spellID = 102051, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frostjaw
		-- Death Knight
			{ spellID = 91797, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Monstrous Blow
			{ spellID = 96294, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Chains of Ice
			{ spellID = 47476, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Strangulate
			{ spellID = 91800, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Gnaw (Ghoul)
			{ spellID = 50435, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Chilblains
			{ spellID = 77606, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Dark Simulacrum
			{ spellID = 91807, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shambling Rush
			{ spellID = 108194, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Asphyxiate
			{ spellID = 115001, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Remorseless Winter
		-- Druid
			{ spellID = 5211, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Mighty Bash
			{ spellID = 33786, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Cyclone
			{ spellID = 22570, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Maim
			{ spellID = 9005, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Pounce
			{ spellID = 81261, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Solar Beam
			{ spellID = 339, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Entangling Roots
			{ spellID = 45334, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Immobilized
			{ spellID = 58180, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Infected Wounds
			{ spellID = 61391, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Typhoon
			{ spellID = 2637, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Hibernate
			{ spellID = 102359, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Mass Entanglement
			{ spellID = 99, unitId = "player", caster = "all", filter = "DEBUFF" },						-- Disorienting Roar
			{ spellID = 127797, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Ursol's Vortex
			{ spellID = 102795, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Bear Hug
			{ spellID = 114238, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Fae Silence
			{ spellID = 50259, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Dazed
			{ spellID = 110698, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Hammer of Justice
			{ spellID = 113004, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Intimidating Roar
			{ spellID = 113056, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Intimidating Roar
			{ spellID = 126458, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Grapple Weapon
		-- Hunter
			{ spellID = 3355, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Freezing Trap
			{ spellID = 24394, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Intimidation
			{ spellID = 19386, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Wyvern Sting
			{ spellID = 34490, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Silencing Shot
			{ spellID = 19503, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Scatter Shot
			{ spellID = 64803, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Entrapment
			{ spellID = 35101, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Concussive Barrage
			{ spellID = 5116, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Concussive Shot
			{ spellID = 13810, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Ice Trap Aura
			{ spellID = 61394, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Glyph of Freezing Trap
			{ spellID = 50519, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Sonic Blast
			{ spellID = 91644, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Snatch
			{ spellID = 90327, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Lock Jaw
			{ spellID = 50245, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Pin
			{ spellID = 90337, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Bad Manner
			{ spellID = 54706, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Venom Web Spray
			{ spellID = 4167, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Web
			{ spellID = 56626, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Sting
			{ spellID = 50541, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Clench
			{ spellID = 1513, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Scare Beast
			{ spellID = 117405, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Binding Shot
			{ spellID = 128405, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Narrow Escape
		-- Priest
			{ spellID = 605, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Dominate Mind
			{ spellID = 64044, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Psychic Horror
			{ spellID = 64058, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Psychic Horror - Disarm
			{ spellID = 8122, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Psychic Scream
			{ spellID = 9484, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shackle Undead
			{ spellID = 87204, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Sin and Punishment
			{ spellID = 87194, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Paralysis
			{ spellID = 15407, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Mind Flay
			{ spellID = 15487, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Silence
			{ spellID = 114404, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Void Tendrils
			{ spellID = 88625, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Holy Word: Chastise
			{ spellID = 113792, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Psychic Terror (Psyfiend)
		-- Shaman
			{ spellID = 76780, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Bind Elemental
			{ spellID = 61882, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Earthquake
			{ spellID = 77505, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Earthquake (Stun Proc)
			{ spellID = 51514, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Hex
			{ spellID = 64695, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Earthgrab (Earth's Grasp)
			{ spellID = 63685, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Freeze (Frozen Power)
			{ spellID = 3600, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Earthbind
			{ spellID = 8056, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frost Shock
			{ spellID = 8034, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frostbrand Attack
			{ spellID = 118905, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Static Charge
			{ spellID = 118345, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shaman Primal Earth Elemental
		-- Warlock
			{ spellID = 89766, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Axe Toss (Felguard)
			{ spellID = 710, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Banish
			{ spellID = 6789, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Mortal Coil
			{ spellID = 118699, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Fear
			{ spellID = 5484, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Howl of Terror
			{ spellID = 6358, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Seduction (Succubus)
			{ spellID = 115268, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Mesmerize (Succubus)
			{ spellID = 30283, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shadowfury
			{ spellID = 24259, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Spell Lock (Felhunter)
			{ spellID = 31117, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Unstable Affliction
			{ spellID = 115782, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Optical Blast (Observer)
			{ spellID = 115268, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Mesmerize (Shivarra)
			{ spellID = 118093, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Disarm (Voidwalker)
			{ spellID = 80240, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Havoc
			{ spellID = 108505, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Archimonde's Vengeance
			{ spellID = 104045, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Sleep (Metamorphosis)
		-- Warrior
			{ spellID = 1715, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Hamstring
			{ spellID = 7922, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Charge Stun
			{ spellID = 20511, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Intimidating Shout
			{ spellID = 132168, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shockwave
			{ spellID = 18498, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Silenced - Gag Order
			{ spellID = 676, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },		-- Disarm
			{ spellID = 12323, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Piercing Howl
			{ spellID = 86346, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Colossus Smash
			{ spellID = 105771, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Warbringer
			{ spellID = 107566, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Staggering Shout
			{ spellID = 107570, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Storm Bolt
			{ spellID = 118895, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Dragon Roar
		-- Monk
			{ spellID = 123393, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Breath of Fire (Glyph of Breath of Fire)
			{ spellID = 126451, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Clash
			{ spellID = 119392, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Charging Ox Wave
			{ spellID = 119381, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Leg Sweep
			{ spellID = 115078, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Paralysis
			{ spellID = 117368, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Grapple Weapon
			{ spellID = 116709, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Spear Hand Strike
			{ spellID = 116706, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Disable
			{ spellID = 123407, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Spinning Fire Blossom
		-- Racials
			{ spellID = 20549, unitId = "player", caster = "all", filter = "DEBUFF" },					-- War Stomp
			{ spellID = 69179, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Arcane Torrent
			{ spellID = 107079, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Quaking Palm
		-- Other
			{ spellID = 94794, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Rocket Fuel Leak
			{ spellID = 82406, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Reversed Shield
			{ spellID = 94549, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Plasma Misfire!
		},
		{
			Name = "FOCUS PVP DEBUFF",
			Enable = true,
			Direction = "LEFT",
			Interval = 6,
			Mode = "ICON",
			Alpha = 1,
--			BarWidth = 150,
			Merge = true,
			Mergewith = "Cooldown",
			Size = 70,
			setPoint = { "TOPRIGHT", "iFilgerFocusBuffs", 0, -22 },
		-- Rogue
			{ spellID = 408, unitId = "focus", caster = "all", filter = "DEBUFF" },						-- Kidney Shot
			{ spellID = 2094, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Blind
			{ spellID = 6770, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Sap
			{ spellID = 88611, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Smoke Bomb
			{ spellID = 1330, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Garrote
			{ spellID = 1776, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Gouge
			{ spellID = 1833, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Cheap Shot
			{ spellID = 113953, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Paralysis
		-- Paladin
			{ spellID = 853, unitId = "focus", caster = "all", filter = "DEBUFF" },						-- Hammer of Justice
			{ spellID = 20066, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Repentance
			{ spellID = 119072, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Holy Wrath
			{ spellID = 10326, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Turn Evil
			{ spellID = 31935, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },		-- Avenger's Shield
			{ spellID = 105593, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Fist of Justice
			{ spellID = 105421, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Blinding Light
		-- Mage
			{ spellID = 55021, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Improved Counterspell
			{ spellID = 118, unitId = "focus", caster = "all", filter = "DEBUFF" },						-- Polymorph
			{ spellID = 44572, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Deep Freeze
			{ spellID = 82691, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Ring of Frost
			{ spellID = 31661, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Dragon's Breath
			{ spellID = 118271, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Combustion Impact
			{ spellID = 102051, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Frostjaw
		-- Death Kinght
			{ spellID = 91797, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Monstrous Blow
			{ spellID = 47476, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Strangulate
			{ spellID = 91800, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Gnaw (Ghoul)
			{ spellID = 108194, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Asphyxiate
			{ spellID = 115001, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Remorseless Winter
		-- Druid
			{ spellID = 5211, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Mighty Bash
			{ spellID = 33786, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Cyclone
			{ spellID = 22570, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Maim
			{ spellID = 9005, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Pounce
			{ spellID = 81261, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Solar Beam
			{ spellID = 2637, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Hibernate
			{ spellID = 99, unitId = "focus", caster = "all", filter = "DEBUFF" },						-- Disorienting Roar
			{ spellID = 102795, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Bear Hug
			{ spellID = 114238, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Fae Silence
			{ spellID = 110698, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Hammer of Justice
			{ spellID = 113004, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Intimidating Roar
			{ spellID = 113056, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Intimidating Roar
			{ spellID = 126458, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Grapple Weapon
		-- Hunter
			{ spellID = 3355, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Freezing Trap
			{ spellID = 24394, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Intimidation
			{ spellID = 19386, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Wyvern Sting
			{ spellID = 34490, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Silencing Shot
			{ spellID = 19503, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Scatter Shot
			{ spellID = 50519, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Sonic Blast
			{ spellID = 50245, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Pin
			{ spellID = 90337, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Bad Manner
			{ spellID = 56626, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Sting
			{ spellID = 1513, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Scare Beast
			{ spellID = 117405, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Binding Shot
		-- Priest
			{ spellID = 605, unitId = "focus", caster = "all", filter = "DEBUFF" },						-- Dominate Mind
			{ spellID = 64044, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },		-- Psychic Horror
			{ spellID = 8122, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Psychic Scream
			{ spellID = 9484, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Shackle Undead
			{ spellID = 87204, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Sin and Punishment
			{ spellID = 87194, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Paralysis
			{ spellID = 15407, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Mind Flay
			{ spellID = 15487, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Silence
			{ spellID = 88625, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Holy Word: Chastise
			{ spellID = 113792, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Psychic Terror (Psyfiend)
		-- Shaman
			{ spellID = 77505, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },		-- Earthquake
			{ spellID = 51514, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Hex
			{ spellID = 118905, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Static Charge
			{ spellID = 118345, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Shaman Primal Earth Elemental
		-- Warlock
			{ spellID = 89766, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Axe Toss (Felguard)
			{ spellID = 6789, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Mortal Coil
			{ spellID = 118699, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Fear
			{ spellID = 5484, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Howl of Terror
			{ spellID = 6358, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Seduction (Succubus)
			{ spellID = 115268, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Mesmerize (Succubus)
			{ spellID = 30283, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Shadowfury
			{ spellID = 24259, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Spell Lock (Felhunter)
			{ spellID = 115782, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Optical Blast (Observer)
			{ spellID = 115268, unitId = "focus", caster = "all", filter = "DEBUFF", absID = true },	-- Mesmerize (Shivarra)
			{ spellID = 104045, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Sleep (Metamorphosis)
		-- Warrior
			{ spellID = 1715, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Hamstring
			{ spellID = 7922, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Charge Stun
			{ spellID = 20511, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Intimidating Shout
			{ spellID = 132168, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Shockwave
			{ spellID = 18498, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Gag Order
			{ spellID = 12323, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Piercing Howl
			{ spellID = 105771, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Warbringer
			{ spellID = 107570, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Storm Bolt
			{ spellID = 118895, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Dragon Roar
		-- Monk
			{ spellID = 123393, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Breath of Fire (Glyph of Breath of Fire)
			{ spellID = 126451, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Clash
			{ spellID = 119392, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Charging Ox Wave
			{ spellID = 119381, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Leg Sweep
			{ spellID = 115078, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Paralysis
			{ spellID = 116709, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Spear Hand Strike
		-- Racials
			{ spellID = 20549, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- War Stomp
			{ spellID = 69179, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Arcane Torrent
			{ spellID = 107079, unitId = "focus", caster = "all", filter = "DEBUFF" },					-- Quaking Palm
		},
	},
	["PVE"] = {
		{
			Name = "Mists of Pandaria Debuffs",
			Enable = false,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			BarWidth = 150,
			Alpha = 1,
			Size = 47,
			setPoint = { "BOTTOMRIGHT", "iFilgerPveDeBuffs", 0, 22 },
		-- Sha of Anger
			{ spellID = 119487, unitId = "player", caster = "all", filter = "DEBUFF" },	--Seethe
			{ spellID = 119626, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Aggressive Behavior
			{ spellID = 119488, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Unleashed Wrath
		--Salys's Warband
			{ spellID = 121787, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Stomp
		-- Imperial Vizier Zor'lok
			{ spellID = 122761, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Exhale
			{ spellID = 122740, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Convert
		-- Blade Lord Ta'yak
			{ spellID = 123474, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Overwhelming Assault
			{ spellID = 123175, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Windstep
		-- Garalon
			{ spellID = 123092, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Pheromones
		-- Wind Lord Mel'Jarak
			{ spellID = 131813, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Wind Bomb
			{ spellID = 121876, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Amber Prison
			{ spellID = 122064, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Corrosive Resin
		-- Amber-Shaper Un'sok
			{ spellID = 122504, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Burning Amber
			{ spellID = 121949, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Parasitic Growth
			-- Mutated Construct
			{ spellID = 122389, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Amber Strike
			{ spellID = 122413, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Fling
			{ spellID = 125498, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Amber Globule
		-- Grand Empress Shek'zeer
			{ spellID = 123707, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Eyes of the Empress
			-- Fixate -- Need to get an ID when MoP is live
			{ spellID = 124097, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Sticky Resin
			{ spellID = 124821, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Poison-Drenched Armor
			{ spellID = 124849, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Consuming Terror
			{ spellID = 124862, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Visions of Demise
			{ spellID = 123845, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Heart of Fear
		-- The Stone Guard
			{ spellID = 116235, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Amethyst Pool
			{ spellID = 130395, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Jasper Chains
		-- Feng the Accursed
			{ spellID = 131788, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Lightning Last
			{ spellID = 131790, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Arcane Shock
			{ spellID = 131792, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Shadowburn
			{ spellID = 116583, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Wildfire Spark
			{ spellID = 116417, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Arcane Resonance
			{ spellID = 118783, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Chains of Shadow
			{ spellID = 115856, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Nullification Barrier
			{ spellID = 115911, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Shroud of Reversal
		-- Garajal the Spiritbinder
			{ spellID = 116166, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Cross Over
			{ spellID = 116000, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Voodoo Dolls
		-- Elegon
			{ spellID = 117878, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Overcharged
			{ spellID = 117949, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Closed Circuit
			{ spellID = 119722, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Energy Cascade
		-- Will of the Emperor
			{ spellID = 116525, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Focused Assault
			{ spellID = 116778, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Focused Defense
			{ spellID = 116829, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Focused Energy
		-- Protector of the Endless
			{ spellID = 117986, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Defiled Ground
			{ spellID = 117398, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Lightning Prison
			{ spellID = 117905, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Corrupted Essence
		-- Tsulong
			{ spellID = 122768, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Dread Shadows
			{ spellID = 122752, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Shadow Breath
			{ spellID = 123011, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Terrorize
		-- Lei Shi
			{ spellID = 123121, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Spray
			{ spellID = 123705, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Scary Fog
		},
		{
			Name = "Cataclysm debuff",
			Enable = false,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			BarWidth = 150,
			Alpha = 1,
			Size = 47,
			setPoint = { "BOTTOMRIGHT", "iFilgerPveDeBuffs", 0, 22 },
		-- Baradin Hold
			{ spellID = 95173, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Consuming Darkness
			{ spellID = 88942, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Meteor Slash / Meteorschlag (Argaloth)
			{ spellID = 97028, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Gaze of Occu'thar
		-- Magmaw
			{ spellID = 91911, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Constricting Chains
			{ spellID = 94679, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Parasitic Infection
			{ spellID = 94617, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Mangle
		-- Omintron Defense System
			{ spellID = 79835, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Poison Soaked Shell
			{ spellID = 91433, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Lightning Conductor
			{ spellID = 91521, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Incineration Security Measure
			{ spellID = 79729, unitId = "focus", caster = "all", filter = "BUFF" },		-- Power Conversion - Arcanotron
			{ spellID = 91547, unitId = "player", caster = "all", filter = "BUFF" },	-- Power Conversion - Arcanotron
		-- Maloriak
			{ spellID = 77699, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Flash Freeze
			{ spellID = 77760, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Biting Chill
			{ spellID = 92988, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Dark Sludge
			{ spellID = 77912, unitId = "target", caster = "all", filter = "BUFF" },	-- Remedy
			{ spellID = 77912, unitId = "focus", caster = "all", filter = "BUFF" },		-- Remedy
		-- Atramedes
			{ spellID = 92423, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Searing Flame
			{ spellID = 92485, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Roaring Flame
			{ spellID = 92407, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Sonic Breath
		-- Chimaeron	
			{ spellID = 82881, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Break
			{ spellID = 89084, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Low Health
		-- Nefarian
			{ spellID = 92053, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Shadow Conductor
			{ spellID = 80627, unitId = "player", caster = "all", filter = "BUFF" },	-- Stolen Power
		--Valiona & Theralion
			{ spellID = 92878, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Blackout
			{ spellID = 86840, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Devouring Flames
			{ spellID = 86622, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Engulfing Magic
			{ spellID = 86013, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Twilight Meteorite
		-- Halfus Wyrmbreaker
			{ spellID = 39171, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Malevolent Strikes
		-- Twilight Ascendant Council
			{ spellID = 82662, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Burning Blood
			{ spellID = 92511, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Hydro Lance
			{ spellID = 82762, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Waterlogged
			{ spellID = 92505, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Frozen
			{ spellID = 92518, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Flame Torrent
			{ spellID = 83099, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Lightning Rod
			{ spellID = 92075, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Gravity Core
			{ spellID = 92488, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Gravity Crush
			{ spellID = 92067, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Static Overload
			{ spellID = 92307, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Frost Beacon
		-- Cho'gall
			{ spellID = 81836, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Corruption: Accelerated
			{ spellID = 82125, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Corruption: Malformation
			{ spellID = 82170, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Corruption: Absolute
			{ spellID = 93200, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Corruption: Sickness
			{ spellID = 86028, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Cho's Blast
			{ spellID = 86029, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Gall's Blast
		-- Sinestra
			{ spellID = 89435, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Wrack
			{ spellID = 87946, unitId = "player", caster = "all", filter = "BUFF" },	-- Essence of the Red
		-- Nezir <Lord of the North Wind>
			{ spellID = 93123, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Wind Chill
			{ spellID = 93131, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Ice Patch
		-- Anshal <Lord of the West Wind>
			{ spellID = 86206, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Soothing Breeze
			{ spellID = 93122, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Toxic Spores
		-- Rohash <Lord of the East Wind>
			{ spellID = 93058, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Slicing Gale
		-- Al'Akir
			{ spellID = 93260, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Ice Storm
			{ spellID = 93271, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Ice Storm
			{ spellID = 93295, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Lightning Rod
			{ spellID = 87873, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Static Shock
			{ spellID = 93279, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Acid Rain
		-- Firelands
			{ spellID = 99532, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Melt Armor
		-- Shannox
			{ spellID = 99840, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Magma Rupture
			{ spellID = 99837, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Crystal Prison Trap 
			{ spellID = 99936, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Jagged Tear
		-- Lord Rhyolith
			{ spellID = 98492, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Eruption
		-- Alysrazor
			{ spellID = 97128, unitId = "player", caster = "all", filter = "BUFF" },	-- Molten Feather
			{ spellID = 98619, unitId = "player", caster = "all", filter = "BUFF" },	-- Wings of Flame
			{ spellID = 99461, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Blazing Power
			{ spellID = 100029, unitId = "player", caster = "all", filter = "DEBUFF" },	-- Alysra's Razor
		-- Beth'tilac
			{ spellID = 99476, unitId = "player", caster = "all", filter = "DEBUFF" },					-- The Widow's Kiss
			{ spellID = 99526, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Fixate (Heroic)
			{ spellID = 99278, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Volatile Poison (Heroic)
		-- Baleroc
			{ spellID = 99256, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Torment
			{ spellID = 99257, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Tormented
			{ spellID = 99252, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Blaze of Glory
			{ spellID = 99262, unitId = "player", caster = "all", filter = "BUFF" },					-- Vital Flame
			{ spellID = 99263, unitId = "player", caster = "all", filter = "BUFF" },					-- Vital Spark
			{ spellID = 99516, unitId = "player", caster = "all", filter = "BUFF" },					-- Countdown
		-- Majordomo Staghelm
			{ spellID = 98450, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Searing Seeds
			{ spellID = 98451, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Burning Orbs
		-- Ragnaros
			{ spellID = 100460, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Blazing Heat
			{ spellID = 101239, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Burning Wound
			{ spellID = 99849, unitId = "player", caster = "all", filter = "DEBUFF", absID = true },	-- Fixate
			{ spellID = 100238, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Magma Trap Vulnerability
			{ spellID = 100594, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Superheated
		-- Morchok
			{ spellID = 103541, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Safe
			{ spellID = 103536, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Warning
			{ spellID = 103534, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Danger
			{ spellID = 108570, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Black Blood of the Earth
			{ spellID = 33661, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Crush Armor
		-- Warlord Zon'ozz
			{ spellID = 103434, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Disrupting Shadows
		-- Yor'sahj the Unsleeping
			{ spellID = 103628, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Deep Corruption
		-- Hagara the Stormbinder
			{ spellID = 109325, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Frostflake
			{ spellID = 105285, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Target
			{ spellID = 105289, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shattered Ice
			{ spellID = 104451, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Ice Tomb
			{ spellID = 107061, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Ice Lance
			{ spellID = 105465, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Lightning Storm
			{ spellID = 105259, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Watery Entrenchment
		-- Ultraxion
			{ spellID = 106498, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Looming Darkness
			{ spellID = 106415, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Twilight Burst
			{ spellID = 109075, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Fading Light
			{ spellID = 105896, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Gift of Life
			{ spellID = 105900, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Essence of Dreams
			{ spellID = 105903, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Source of Magic
		--Warmaster Blackhorn
			{ spellID = 109204, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Twilight Barrage
			{ spellID = 108043, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Sunder Armor
			{ spellID = 108046, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shockwave
			{ spellID = 107567, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Brutal Strike
			{ spellID = 107558, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Degeneration
		--Spine of Deathwing
			{ spellID = 105479, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Searing Plasma
			{ spellID = 105490, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Fiery Grip
			{ spellID = 105563, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Grasping Tendrils
		--Madness of Deathwing
			{ spellID = 109597, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Shrapnel
			{ spellID = 105841, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Degenerative Bite
			{ spellID = 105445, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Blistering heat
			{ spellID = 109603, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Tetanus
			{ spellID = 108649, unitId = "player", caster = "all", filter = "DEBUFF" },					-- Corrupting Parasite
		},
		{
			Name = "WotLK debuff",
			Enable = false,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			BarWidth = 150,
			Alpha = 1,
			Size = 47,
			setPoint = { "BOTTOMRIGHT", "iFilgerPveDeBuffs", 0, 22 },
		-- Deathbringer Saurfang
			{ spellID = 72293, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Mark of the Fallen Champion
		-- Festergut
			{ spellID = 72103, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Inoculated
		-- Rotface
			{ spellID = 71224, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Mutated Infection
		-- Professor Putricide
			{ spellID = 72856, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Unbound Plague
			{ spellID = 73117, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Plague Sickness
			{ spellID = 70353, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Gas Variable
			{ spellID = 70352, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Ooze Variable
		-- Bloodqueen Lana'thel
			{ spellID = 71340, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Pact of the Darkfallen
			{ spellID = 71861, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Swarming Shadows
			{ spellID = 71473, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Essence of the Blood Queen
		-- Sindragosa
			{ spellID = 71053, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Frost Bomb
			{ spellID = 69766, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Instability
			{ spellID = 69762, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Unchained Magic
			{ spellID = 70128, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Mystic Buffet
		-- The Lich King
			{ spellID = 73912, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Necrotic Plague
		-- Halion
			{ spellID = 74562, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Fiery Combustion
			{ spellID = 74792, size = 68, unitId = "player", caster = "all", filter = "DEBUFF" },		-- Soul Consumption
		},
	},
	["TANKS"] = {
		{
			Name = "Tanks CDs",
			Enable = true,
			Direction = "HORIZONTAL",
			Interval = 4,
			Mode = "ICON",
			BarWidth = 150,
			Alpha = 1,
			Size = 47,
			setPoint = { "BOTTOM", "iFilgerTanksCDs", 0, 22 },
		-- Priest
			{ spellID = 47788, unitId = "target", caster = "all", filter = "BUFF" },	-- Guardian Spirit
			{ spellID = 33206, unitId = "target", caster = "all", filter = "BUFF" },	-- Pain Suppression
		-- Paladin
			{ spellID = 25771, unitId = "target", caster = "all", filter = "DEBUFF" },	-- Forbearance
			{ spellID = 642, unitId = "target", caster = "all", filter = "BUFF" },		-- Divine Shield
			{ spellID = 1022, unitId = "target", caster = "all", filter = "BUFF" },		-- Hand of Protection
			{ spellID = 498, unitId = "target", caster = "all", filter = "BUFF" },		-- Divine Protection
			{ spellID = 31850, unitId = "target", caster = "all", filter = "BUFF" },	-- Ardent Defender
			{ spellID = 86659, unitId = "target", caster = "all", filter = "BUFF" },	-- Guardian of Ancient Kings
		-- Druid
			{ spellID = 61336, unitId = "target", caster = "all", filter = "BUFF" },	-- Survival Instincts
			{ spellID = 22812, unitId = "target", caster = "all", filter = "BUFF" },	-- Barkskin
			{ spellID = 22842, unitId = "target", caster = "all", filter = "BUFF" },	-- Frenzied Regeneration
		-- Warrior
			{ spellID = 55694, unitId = "target", caster = "all", filter = "BUFF" },	-- Enraged Regeneration
			{ spellID = 97462, unitId = "target", caster = "all", filter = "BUFF" },	-- Rallying Cry
			{ spellID = 871, unitId = "target", caster = "all", filter = "BUFF" },		-- Shield Wall
			{ spellID = 23920, unitId = "target", caster = "all", filter = "BUFF" },	-- Spell Reflection
			{ spellID = 2565, unitId = "target", caster = "all", filter = "BUFF" },		-- Shield Block
			{ spellID = 12975, unitId = "target", caster = "all", filter = "BUFF" },	-- Last Stand
		-- Death Knight
			{ spellID = 48707, unitId = "target", caster = "all", filter = "BUFF" }, 	-- Anti-Magic Shell
			{ spellID = 49222, unitId = "target", caster = "all", filter = "BUFF" }, 	-- Bone Shield
			{ spellID = 48792, unitId = "target", caster = "all", filter = "BUFF" }, 	-- Icebound Fortitude
			{ spellID = 55233, unitId = "target", caster = "all", filter = "BUFF" }, 	-- Vampiric Blood
			{ spellID = 49039, unitId = "target", caster = "all", filter = "BUFF" }, 	-- Lichborne
		},
		{
			Name = "Tanks CDs focus",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			BarWidth = 150,
			Alpha = 1,
			Size = 47,
			setPoint = { "BOTTOMRIGHT", "iFilgerTanksCDsFocus", 0, 22 },
		-- Priest
			{ spellID = 47788, unitId = "focus", caster = "all", filter = "BUFF" },		-- Guardian Spirit
			{ spellID = 33206, unitId = "focus", caster = "all", filter = "BUFF" },		-- Pain Suppression
		-- Paladin
			{ spellID = 25771, unitId = "focus", caster = "all", filter = "DEBUFF" },	-- Forbearance
			{ spellID = 642, unitId = "focus", caster = "all", filter = "BUFF" },		-- Divine Shield
			{ spellID = 1022, unitId = "focus", caster = "all", filter = "BUFF" },		-- Hand of Protection
			{ spellID = 498, unitId = "focus", caster = "all", filter = "BUFF" },		-- Divine Protection
			{ spellID = 31850, unitId = "focus", caster = "all", filter = "BUFF" },		-- Ardent Defender
			{ spellID = 86659, unitId = "focus", caster = "all", filter = "BUFF" },		-- Guardian of Ancient Kings
		-- Druid
			{ spellID = 61336, unitId = "focus", caster = "all", filter = "BUFF" },		-- Survival Instincts
			{ spellID = 22812, unitId = "focus", caster = "all", filter = "BUFF" },		-- Barkskin
			{ spellID = 22842, unitId = "focus", caster = "all", filter = "BUFF" },		-- Frenzied Regeneration
		-- Warrior
			{ spellID = 55694, unitId = "focus", caster = "all", filter = "BUFF" },		-- Enraged Regeneration
			{ spellID = 97462, unitId = "focus", caster = "all", filter = "BUFF" },		-- Rallying Cry
			{ spellID = 871, unitId = "focus", caster = "all", filter = "BUFF" },		-- Shield Wall
			{ spellID = 23920, unitId = "focus", caster = "all", filter = "BUFF" },		-- Spell Reflection
			{ spellID = 2565, unitId = "focus", caster = "all", filter = "BUFF" },		-- Shield Block
			{ spellID = 12975, unitId = "focus", caster = "all", filter = "BUFF" },		-- Last Stand
		-- Death Knight
			{ spellID = 48707, unitId = "focus", caster = "all", filter = "BUFF" }, 	-- Anti-Magic Shell
			{ spellID = 49222, unitId = "focus", caster = "all", filter = "BUFF" }, 	-- Bone Shield
			{ spellID = 48792, unitId = "focus", caster = "all", filter = "BUFF" }, 	-- Icebound Fortitude
			{ spellID = 55233, unitId = "focus", caster = "all", filter = "BUFF" }, 	-- Vampiric Blood
			{ spellID = 49039, unitId = "focus", caster = "all", filter = "BUFF" }, 	-- Lichborne
		},
		{
			Name = "Tanks Boss Debuff",
			Enable = true,
			Direction = "LEFT",
			Interval = 4,
			Mode = "ICON",
			BarWidth = 150,
			Alpha = 1,
			Size = 37,
			setPoint = { "BOTTOMRIGHT", "iFilgerTanksCDs", 0, 22 },
			{ spellID = 1160, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Demoralizing Shout
			{ spellID = 6343, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Thunderclap
			{ spellID = 113746, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Weakened Armor
			{ spellID = 48484, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Infected Wounds
			{ spellID = 99, unitId = "target", caster = "all", filter = "DEBUFF" },			-- Demoralizing Roar
			{ spellID = 55095, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Frost Fever
			{ spellID = 59879, unitId = "target", caster = "player", filter = "DEBUFF" },	-- Blood Plague
			{ spellID = 26017, unitId = "target", caster = "all", filter = "DEBUFF" },		-- Vindication
		},
	},
}
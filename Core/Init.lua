local AddOnName, Engine = ...
Engine[1] = {} -- I
Engine[2] = {} -- C
Engine[3] = {} -- L

iFilger = Engine

local I, C, L = unpack(select(2, ...))

I.Noop = function() return end
I.MyName = UnitName('player')
I.MyClass = select(2, UnitClass('player'))
I.Client = GetLocale() 
I.Resolution = GetCVar('gxResolution')
I.ScreenHeight = tonumber(strmatch(I.Resolution, '%d+x(%d+)'))
I.ScreenWidth = tonumber(strmatch(I.Resolution, '(%d+)x+%d'))
I.Version = GetAddOnMetadata(AddOnName, 'Version')
I.Title = select(2, GetAddOnInfo(AddOnName))
I.InCombat = UnitAffectingCombat('player')
I.MyRealm = GetRealmName()
I.UIScale = UIParent:GetScale()

local Color = RAID_CLASS_COLORS[I.MyClass]

C['General'] = {
	['BackdropColor'] = { .075, .075, .075, .6 },
	['BorderColorConfig'] = { .58, .58, .58 },
	['BorderColorHeader'] = { .39, .39, .39 },
	['BorderColorTab'] = { .2, .2, .2 },
	['BorderColor'] = { .1, .1, .1, 1 },
	['ClassColor'] = { Color.r, Color.g, Color.b },
	['White'] = {1, 1, 1},
	['Red'] = {1, 0, 0},
}

if Tukui or ElvUI or AsphyxiaUI or DuffedUI then
	local Frame = CreateFrame('Frame')
	Frame:SetTemplate()
	C['General']['BackdropColor'] = { Frame:GetBackdropColor() }
	C['General']['BorderColor'] = { Frame:GetBackdropBorderColor() }
end
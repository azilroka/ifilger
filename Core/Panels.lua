local I, C, L = unpack(select(2, ...))

local iFilger = {}
local Color = RAID_CLASS_COLORS[select(2, UnitClass("player"))]

I.MoverFrames = {}

I.CreateMover = function(name, w, h, anchor, x, y, color, text)
	local frame = CreateFrame("Frame", name, UIParent) -- Ice Block / smoke / ShS...
	I.CreateMoverFrames(frame, w, h, anchor, x, y, color, text)
	tinsert(I.MoverFrames, frame)
end

I.CreateMoverFrames = function(frame, w, h, anchor, x, y, color, text)
	frame:CreatePanel("Transparent", w, h, anchor, "UIParent", "CENTER", x, y)
	frame:SetMovable(true)
	if color == "class" then
		frame:SetBackdropBorderColor(Color.r, Color.g, Color.b)
	else
		frame:SetBackdropBorderColor(1, 0, 0)
	end
	frame:SetFrameLevel(2)
	frame:SetFrameStrata("HIGH")
	frame:FontString('text', C['Media']['Pixel'], 12, "MONOCHROMEOUTLINE")
	frame.text:SetPoint("CENTER")
	frame.text:SetText("Move "..text)
	frame:Hide()
end

local Filger_Panels = C.Filger_Panels

for i = 1, #Filger_Panels["ALL"] do
	local panel = Filger_Panels["ALL"][i];
	I.CreateMover(panel.name, panel.w, panel.h, panel.anchor, panel.x, panel.y, "red", panel.text)
end

for i = 1, #Filger_Panels[I.MyClass] do
	local panel = Filger_Panels[I.MyClass][i]
	I.CreateMover(panel.name, panel.w, panel.h, panel.anchor, panel.x, panel.y, "class", panel.text)
end
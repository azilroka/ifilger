local I, C, L = unpack(select(2, ...))

C['Media'] = {
	['Font'] = [=[Interface\Addons\iFilger\media\fonts\normal_font.ttf]=],

	['Taiwan'] = [=[Fonts\bLEI00D.ttf]=],
	['Korean'] = [=[Fonts\2002.TTF]=],
	['Chinese'] = [=[Fonts\ARKai_T.TTF]=],

	['Pixel'] = [=[Interface\Addons\iFilger\media\fonts\pixel_font.ttf]=],
	['NormTex'] = [[Interface\AddOns\iFilger\media\textures\normTex]],
	['GlowTex'] = [[Interface\AddOns\iFilger\media\textures\glowTex]],
	['Blank'] = [[Interface\AddOns\iFilger\media\textures\blank]],

	['BorderColor'] = C.General.BorderColor or { .5, .5, .5, 1 },
	['BackdropColor'] = C.General.BackdropColor or { .1, .1, .1, 1 },
}

if I.Client == 'zhTW' then
	C['Media']['Font'] = C['Media']['Taiwan']
elseif I.Client == 'koKR' then
	C['Media']['Font'] = C['Media']['Korean']
elseif I.Client == 'zhCN' then
	C['Media']['Font'] = C['Media']['Chinese']
end
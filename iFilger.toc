﻿## Interface: 50400
## Author: Azilroka
## Title: |cff1784d1iFilger|r
## Notes: Minimal buff/debuff/proc tracking
## Credits: Nils Ruesch, Shestak, Affli, Garagar, hidekki, Sapz, Asphyxia, mTx, SinaC, Pat, Elv, Tukz, FourOne, Jasje, Ildyria
## OptionalDeps: Tukui, ElvUI, AsphyxiaUI, DuffedUI
## Version: 6.04
## Notes: Power Aura Like, skinned for Tukui / ElvUI.
## SavedVariables: iFilgerConfigUISV
## SavedVariablesPerCharacter: iFilgerConfigUISVPC, iFilgerConfigVersion
## X-Tukui-ProjectID: 83
## X-Tukui-ProjectFolders: iFilger

iFilger.xml
